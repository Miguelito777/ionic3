import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
//import { ListPage } from '../pages/list/list';
import { PensionAlimenticia } from '../pages/pensionAlimenticia/pensionAlimenticia';
import { AntecedentesPenales } from '../pages/antecedentesPenales/antecedentesPenales';
import { Directorio } from '../pages/directorio/directorio';
import { Ubicacion } from '../pages/ubicacion/ubicacion';
import { Amparos } from '../pages/amparos/amparos';
import { Antejuicios } from '../pages/antejuicios/antejuicios';
import { Audiencias } from '../pages/audiencias/audiencias';
import { Integracion } from '../pages/csj/integracion';
import { ContactoPage } from '../pages/contacto/contacto';
//Splash ->import { timer } from 'rxjs/observable/timer';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  //showSplash = true; // Muestra Animación

  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any, icon: string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Inicio', component: HomePage, icon:'home'},
      { title: 'Integración CSJ', component: Integracion, icon:'settings' },
      { title: 'Ubicación Juzgados', component: Ubicacion, icon:'map' },
      { title: 'Directorio', component: Directorio, icon:'bookmarks'},
      { title: 'Antecedentes Penales', component: AntecedentesPenales, icon:'people' },
      { title: 'Pensión Alimenticia', component: PensionAlimenticia, icon:'restaurant' },
      { title: 'Amparos', component: Amparos, icon:'clipboard'},
      { title: 'Antejuicios', component: Antejuicios, icon:'document'},
      { title: 'Audiencias', component: Audiencias, icon:'mic'},
      { title: 'Contacto', component: ContactoPage, icon:'phone-portrait'}
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      //Splashtimer(3000).subscribe(() => this.showSplash = false);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
