import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { PensionAlimenticia } from '../pages/pensionAlimenticia/pensionAlimenticia';
import { AntecedentesPenales } from '../pages/antecedentesPenales/antecedentesPenales';
import { Directorio } from '../pages/directorio/directorio';
import { Ubicacion } from '../pages/ubicacion/ubicacion';
import { Amparos } from '../pages/amparos/amparos';
import { Antejuicios } from '../pages/antejuicios/antejuicios';
import { Audiencias } from '../pages/audiencias/audiencias';
import { Integracion } from '../pages/csj/integracion';
import { Dependencia } from '../pages/dependencia/dependencia';
import { Edificio } from '../pages/edificios/edificios';
import { Amparo } from '../pages/amparos/amparo';
import { Actuaciones } from '../pages/amparos/actuaciones';
import { InfoGeneral } from '../pages/amparos/infoGeneral';
import { TercerosInteresados } from "../pages/amparos/tercerosInteresados";
import { ListAudiencias } from '../pages/audiencias/listAudiencias';
import { Antejuicio } from '../pages/antejuicios/antejuicio';
import { Geolocation } from '@ionic-native/geolocation';
import { csj } from '../pages/csj/csj';
import { Penal } from '../pages/csj/penal';
import { Civil } from '../pages/csj/civil';
import { AmparoAntejuicio } from '../pages/csj/amparoAntejuicio';
import { CallNumber } from '@ionic-native/call-number';
import { ContactoPage } from '../pages/contacto/contacto';

// Servicios CAPE
import { Cape } from '../pages/cape/cape';
import { SolicitarCarencia } from '../pages/cape/solicitarCarencia';
import { InvalidRecipt } from '../pages/cape/invalidRecipt';
import { RequestWithout } from '../pages/cape/giveRequestWithout';
import { ValidarCAPE } from '../pages/cape/validarCAPE';
import { BoletaCarencia } from '../pages/cape/boletaCarencia';
import { Seguimiento } from '../pages/cape/seguimiento';
import { TimelineComponent } from '../pages/cape/timeline';
import { TimelineTimeComponent } from '../pages/cape/timeline';
import { TimelineItemComponent } from '../pages/cape/timeline';
import { SeguimientoTimeLine } from '../pages/cape/SeguimientoTimeLine';
import { scanQR } from '../pages/cape/scanQR';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RestProvider } from '../providers/rest/rest';

import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'; 

import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { CustomEventTitleFormatterProvider } from '../providers/custom-event-title-formatter/custom-event-title-formatter';
import { CustomDateFormatterProvider } from '../providers/custom-date-formatter/custom-date-formatter';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CalendarModule, CalendarDateFormatter, CalendarEventTitleFormatter } from 'angular-calendar';
import { CalendarWeekHoursViewModule } from 'angular-calendar-week-hours-view';
import localeDe from '@angular/common/locales/es-GT';
import { registerLocaleData } from '@angular/common';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { LaunchNavigator } from '@ionic-native/launch-navigator';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './../providers/rest/httpInterceptor';

registerLocaleData(localeDe);


import { DemoModule } from '../modules/angular-calendar/module';

// modulos para generar pdf
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

import { AppAvailability } from '@ionic-native/app-availability';
import { IonicStorageModule } from '@ionic/storage';

import { SelectSearchableModule } from 'ionic-select-searchable';
import { EmailComposer } from '@ionic-native/email-composer';

import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    PensionAlimenticia,
    AntecedentesPenales,
    Directorio,
    Ubicacion,
    Amparos,
    Antejuicios,
    Audiencias,
    Integracion,
    Dependencia,
    Edificio,
    Amparo,
    Actuaciones,
    InfoGeneral,
    TercerosInteresados,
    ListAudiencias,
    Antejuicio,
    Cape,
    SolicitarCarencia,
    RequestWithout,
    InvalidRecipt,
    ValidarCAPE,
    BoletaCarencia,
    csj,
    Penal,
    Civil,
    AmparoAntejuicio,
    ContactoPage,
    Seguimiento,
    TimelineComponent,
    TimelineItemComponent,
    TimelineTimeComponent,
    SeguimientoTimeLine,
    scanQR
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      tabsPlacement: 'top',
      backButtonText: 'Atrás'
    }),
    CalendarModule.forRoot(),
    NgbModalModule.forRoot(),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    //CalendarModule,
    AgmDirectionModule,
    BrowserAnimationsModule,
    CalendarWeekHoursViewModule,
    DemoModule,
    SelectSearchableModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBpoLWt1A0D59g1Z_Dg7fNQpFsV95OccQY'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    AntecedentesPenales,
    PensionAlimenticia,
    Directorio,
    Ubicacion,
    Amparos,
    Antejuicios,
    Audiencias,
    Integracion,
    Dependencia,
    Edificio,
    Amparo,
    Actuaciones,
    InfoGeneral,
    TercerosInteresados,
    ListAudiencias,
    Antejuicio,
    Cape,
    SolicitarCarencia,
    RequestWithout,
    InvalidRecipt,
    ValidarCAPE,
    BoletaCarencia,
    csj,
    Penal,
    Civil,
    AmparoAntejuicio,
    ContactoPage,
    Seguimiento,
    TimelineComponent,
    TimelineItemComponent,
    TimelineTimeComponent,
    SeguimientoTimeLine,
    scanQR
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LaunchNavigator,
    CallNumber,
    File,
    FileOpener,
    Geolocation,
    AppAvailability,
    EmailComposer,
    QRScanner,
    {
      provide: HTTP_INTERCEPTORS, 
      useClass: AuthInterceptor,
      multi: true
    },
    RestProvider,
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatterProvider
    },
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatterProvider
    }
  ]
})
export class AppModule {}
