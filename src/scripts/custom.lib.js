module.exports = {
    copyAssets: {
        src: ['/assets/**/*'],
        dest: '/assets'
    },
    copyIndexContent: {
        src: ['/index.html', '/manifest.json', '/service-worker.js'],
        dest: ''
    },
    copyFonts: {
        src: ['/node_modules/ionicons/dist/fonts/**/*', '/node_modules/ionic-angular/fonts/**/*'],
        dest: '/assets/fonts'
    },
    copyPolyfills: {
        src: ['/node_modules/ionic-angular/polyfills/polyfills.js'],
        dest: ''
    },
    copySwToolbox: {
        src: ['/node_modules/sw-toolbox/sw-toolbox.js'],
        dest: ''
    },
    copyCalendarCss: {
        src: './node_modules/fullcalendar/dist/fullcalendar.min.css',
        dest: ''
    }
}