import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef
} from '@angular/core';
import {
  startOfDay,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction//,
  //CalendarEventTimesChangedEvent
} from 'angular-calendar';
import { NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  green: {
    primary: '#509624',
    secondary: '#A9CC94'
  },
  gray: {
    primary: '#8B8D8B',
    secondary: '#D1D4D1'
  }
};

@Component({
  //moduleId: __moduleName,
  selector: 'mwl-demo-component',
  templateUrl: 'template.html',
  //styleUrls: ['styles.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DemoComponent {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  view: string = 'month';
  viewDate: Date = new Date();
  locale: string = 'es-GT';
  isDragging = false;
  currentMont: number;
  despacho: 0;
  estado: '';
  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
    /*{
      start: subDays(startOfDay(new Date()), 1),
      end: addDays(new Date(), 1),
      title: 'PRIMERA DECLARACION LIBRE!!!!! POR EL DELITO DE APROPIACION Y RETENCION INDEBIDAS ',
      color: colors.red,
      actions: this.actions
    },
    {
      start: startOfDay(new Date()),
      title: 'SINDICADO LIBRE!!!!! POR HOMICIDIO',
      color: colors.yellow,
      actions: this.actions
    },
    {
      start: subDays(endOfMonth(new Date()), 3),
      end: addDays(endOfMonth(new Date()), 3),
      title: 'PRIMERA DECLARACION LIBRE!!! ',
      color: colors.blue
    },
    {
      start: addHours(startOfDay(new Date()), 2),
      end: new Date(),
      title: 'ETAPA INTERMEDIA (LIBRE)',
      color: colors.yellow,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    }*/
  ];

  activeDayIsOpen: boolean = true;

  constructor(private modal: NgbModal, private navParams: NavParams, public loadingCtrl: LoadingController, private RestProvider: RestProvider) {
    this.despacho = navParams.data.idDespacho;
    this.estado = navParams.data.estado;
    this.getAudiencias();
    //console.log(subDays(startOfDay(new Date()), 1));
    //console.log(addDays(new Date(), 1));
    this.currentMont = this.viewDate.getMonth()+1;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  /*eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }*/

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  getAudiencias(): void {
    let monthCurrent = this.viewDate.getMonth() + 1;
    let yearCurrent = this.viewDate.getFullYear();
    let daysInMonth = this.daysInMonth(monthCurrent, yearCurrent);
    let loading = this.loadingCtrl.create({ content: 'Audiencias...' });
    let dateStart;
    let dateEnd;
    if(monthCurrent < 10){
      dateStart = '01-'+'0'+monthCurrent+'-'+yearCurrent;
      dateEnd = daysInMonth+'-'+'0'+monthCurrent+'-'+yearCurrent;
    }else{
      dateStart = '01-'+monthCurrent+'-'+yearCurrent;
      dateEnd = daysInMonth+'-'+monthCurrent+'-'+yearCurrent;
    }
    loading.present();
    this.RestProvider.getAudiencias(this.despacho, dateStart, dateEnd, this.estado).subscribe(
      data => {
        loading.dismiss();
        for (var i in data) {
          let dateParts = data[i].AUD_EXP_FEC_INICIO.split("/");
          let dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
    
          let color: any;
          switch (data[i].ESTADO) {
            case 'Celebrada':
              color = colors.green;
              break;
            case 'Error en Programación':
              color = colors.gray;
              break;
            case 'Suspendida Iniciada':
              color = colors.red;
              break;
            case 'Programada':
              color = colors.yellow;
              break;
            default:
              color = colors.blue;
              break;
          }
          let hora =data[i].HORA.split(':');
          let newDate = {
            //start: startOfDay(),
            start: addHours(startOfDay(dateObject), parseInt(hora[0]) ),
            title: data[i].EXPEDIENTE_NUMERO_UNICO + ' - ' + data[i].TIPO +' - Hora: '+data[i].HORA,
            color: color,
            actions: this.actions
          }
          this.events.push(newDate);
          this.refresh.next();
        }
      },
      error => {
        console.log(error);
      }
    )  



    /*const subFn: any = {
      day: subDays,
      week: subWeeks,
      month: subMonths
    }[this.view];

    this.viewDateChange.emit(subFn(this.viewDate, 1));*/
  }
  daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }
}