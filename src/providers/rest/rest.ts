import { HttpClient, HttpErrorResponse, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, retry } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { LoadingController } from 'ionic-angular';

//import { Antecedente } from '../../pages/antecedentesPenales/Antecedente';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
  //Desarrollo
  //baseUrl: string = "http://192.168.0.30:8080/ojapp_dmz/dmz/";
  baseUrlCAPE: string = "http://wsdesacit:8080/CAPE_DMZ/webapi/";
  //servicesCIDEJ: String = "http://192.168.0.30:8080/ojapp_dmz/dmz/";
  //dmzCIDEJDesa:String="http://192.168.0.30:8080/ojapp_dmz/dmz/";
  //ojAppUrl:String="http://localhost:8080/OrganismoJudicialGuatemala/webapi/";


  //Producción
  //baseUrlCAPE:string = "http://serviciosrest.oj.gob.gt:8080/CAPE_DMZ/webapi/";
  baseUrl:string = "http://serviciosrest.oj.gob.gt:8080/ojapp_dmz/dmz/";
  servicesCIDEJ: String = "http://serviciosrest.oj.gob.gt:8080/ojapp_dmz/dmz/";
  dmzCIDEJDesa:String="http://serviciosrest.oj.gob.gt:8080/ojapp_dmz/dmz/";
  //ojAppUrl:String="http://serviciosrest.oj.gob.gt:8080/OrganismoJudicialGuatemala/webapi/";
  ojAppUrl:String="http://serviciosrest.oj.gob.gt:8080/ojapp_dmz/dmz/";
  
  
  directorioJdos: any;
  private anios: any;
  private departamentos: any;
  private municipios: any;
  private dependencias: any;
  private estado: any;
  private columnas: any;
  private audiencias: any;
  private amparo: any;

  headers = new HttpHeaders();




  constructor(public http: HttpClient, public loadingCtrl: LoadingController) {

    //config.headers.Authorization = 'Bearer ' +fecha256;

    this.anios = [
      { anio: 2010, },
      { anio: 2011, },
      { anio: 2012, },
      { anio: 2013, },
      { anio: 2014, },
      { anio: 2015, },
      { anio: 2016, },
      { anio: 2017, },
      { anio: 2018, }
    ];
    this.departamentos = [
      { idDepartamento: '0', nombreDepartamento: 'Guatemala' },
      { idDepartamento: '1', nombreDepartamento: 'Solola' },
      { idDepartamento: '2', nombreDepartamento: 'Quetzaltenango' },
      { idDepartamento: '3', nombreDepartamento: 'Totonicapán' },
      { idDepartamento: '4', nombreDepartamento: 'Huehuetenango' },
      { idDepartamento: '5', nombreDepartamento: 'San Marcos' },
      { idDepartamento: '6', nombreDepartamento: 'Quiché' },
      { idDepartamento: '7', nombreDepartamento: 'Retalhuleu' },
      { idDepartamento: '8', nombreDepartamento: 'Suchitepequez' }
    ];
    this.municipios = [
      { idmunicipio: '0', nombreMunicipio: 'Guatemala' },
      { idmunicipio: '1', nombreMunicipio: 'Amatitlán' },
      { idmunicipio: '2', nombreMunicipio: 'Mixco' },
      { idmunicipio: '3', nombreMunicipio: 'Villa Nueva' }
    ];
    this.dependencias = [
      { idDependencia: '1', nombreDependencia: 'CÁMARA PENAL DE LA CORTE SUPREMA DE JUSTICIA' },
      { idDependencia: '2', nombreDependencia: 'JUZGADO DE PAZ PENAL DE FALTAS DE TURNO' },
      { idDependencia: '3', nombreDependencia: 'JUZGADO DE PRIMERA INSTANCIA PENAL TURNO' },
      { idDependencia: '4', nombreDependencia: 'JUZGADO PRIMERO PLURIPERSONAL DE PAZ PENAL' },
      { idDependencia: '5', nombreDependencia: 'JUZGADO SEGUNDO DE PRIMERA INSTANCIA PENAL' },
      { idDependencia: '6', nombreDependencia: 'JUZGADO TERCERO DE PRIMERA INSTANCIA PENAL' },
      { idDependencia: '7', nombreDependencia: 'JUZGADO CUARTO DE PRIMERA INSTANCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'JUZGADO QUINTO DE PRIMERA INSTANCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'JUZGADO SEXTO DE PRIMERA INSTANCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'JUZGADO SÉPTIMO DE PRIMERA INSTANCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'JUZGADO OCTAVO DE PRIMERA INSTANCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'JUZGADO NOVENO DE PRIMERA INSTANCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'JUZGADO DÉCIMO DE PRIMERA INSTANCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'JUZGADO UNDÉCIMO DE PRIMERA INSTANCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'TRIBUNAL SEGUNDO DE SENTENCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'TRIBUNAL TERCERO DE SENTENCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'TRIBUNAL CUARTO DE SENTENCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'TRIBUNAL QUINTO DE SENTENCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'TRIBUNAL SEXTO DE SENTENCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'TRIBUNAL SÉPTIMO DE SENTENCIA PENAL' },
      { idDependencia: '8', nombreDependencia: 'TRIBUNAL OCTAVO DE SENTENCIA PENAL' }
    ];
    this.estado = [
      { estado: 'Programada' },
      { estado: 'Celebrada' },
      { estado: 'Cancelada' },
      { estado: 'Suspendida' },
      { estado: 'Reprogramada' },
      { estado: 'Prorrogada' },
      { estado: 'Anulada' },
      { estado: 'Todos' }
    ];
    this.columnas = [
      { descripcion: 'Fecha' },
      { descripcion: 'Hora Inicio' },
      { descripcion: 'Estado' },
      { descripcion: 'Núm Único Expediente' },
      { descripcion: 'Descripción' }
    ];
    this.audiencias = [
      {
        Fecha: "23-04-2018",
        horaInicio: "8:00",
        Estado: "Programada",
        numeroUnicoDeExpediente: "01078-2006-05599",
        descripcion: "DEVOLUCIÓN DE ARMAS DE FUEGO OJOOO 12:15 HORAS!"
      },
      {
        Fecha: "24-04-2018",
        horaInicio: "10:00",
        Estado: "Programada",
        numeroUnicoDeExpediente: "***",
        descripcion: "AUDIENCIA UNILATERAL"
      },
      {
        Fecha: "25-04-2018",
        horaInicio: "12:00",
        Estado: "Programada",
        numeroUnicoDeExpediente: "***",
        descripcion: "AUDIENCIA UNILATERAL"
      },
      {
        Fecha: "26-04-2018",
        horaInicio: "13:30",
        Estado: "Programada",
        numeroUnicoDeExpediente: "***",
        descripcion: "AUDIENCIA UNILATERAL"
      },
      {
        Fecha: "27-04-2018",
        horaInicio: "15:00",
        Estado: "Programada",
        numeroUnicoDeExpediente: "***",
        descripcion: "AUDIENCIA UNILATERAL"
      },
      {
        Fecha: "28-04-2018",
        horaInicio: "16:30",
        Estado: "Programada",
        numeroUnicoDeExpediente: "***",
        descripcion: "AUDIENCIA UNILATERAL"
      },
      {
        Fecha: "29-04-2018",
        horaInicio: "18:00",
        Estado: "Programada",
        numeroUnicoDeExpediente: "***",
        descripcion: "AUDIENCIA UNILATERAL"
      },
      {
        Fecha: "30-04-2018",
        horaInicio: "8:30",
        Estado: "Programada",
        numeroUnicoDeExpediente: "01078-2017-00621",
        descripcion: "SOLICITUD DE PRIMERA DECLARACIÓN CONSTITUCION DE QUERELLANTE Y DISCREPANCIA"
      }
    ];
    this.amparo = {
      INFORMACION_GENERAL: [
        {
          ANIO: "2017",
          NOAMPARO: "2",
          OFICIAL: "2",
          FECHAADMISION: "03/01/2017",
          AUTORIDAD: "Fiscal General De La República Y Jefe Del  Ministerio Público",
          AMPARISTA: "Alimentos Y Bebidas Atlantida, Sociedad Anónima, Por Medio De Su Mandataria Especial Judicial Con Representación, Abogada Ana Raquel Villeda Osorio",
          OTORGADOS_PROVISIONALMENTE: "0",
          DENEGADOS_PROVISIONALMENTE: "0",
          INSTANCIA1: "",
          ANIOS: "2012",
          NUMEROS: "851",
          INSTANCIA2: "18002",
          ANIO2: "2012",
          NUMERO2: "851",
          DEPENDENCIA: "",
          FECHADEP: "",
          DOCUMENTODEP: "",
          ACTO_RECLAMADO: "Resolución De Fecha 28-11-16 Que Declaro Sin Lugar La Solicitud De Recusación Y Apartamiento De Los Fiscales A Cargo De La Investigación, Que Se Deriva De La Causa Penal En La Cual Se Le Acusa De Elaboración Peligrosa De Sustancias Alimenticias O Terapéuticas."
        }
      ],
      ACTUACIONES: [
        {
          FECHA: "01/03/2017",
          LINEA: "12",
          ACTUACION: "Fecha Resolucion De Interposicion De Apelacion Simple De Auto"
        },
        {
          FECHA: "21/02/2017",
          LINEA: "8",
          ACTUACION: "Se Entrego Al Notificador Para Notificar Primera Audiencia"
        },
        {
          FECHA: "30/01/2017",
          LINEA: "5",
          ACTUACION: "Fecha Remisión Expediente A Vocalía Para Análisis De Primera Audiencia"
        },
        {
          FECHA: "03/01/2017",
          LINEA: "1",
          ACTUACION: "Resolucion De Admision Simple"
        }
      ],
      TERCEROS_INTERESADOS: [
        { NOMBRE: "Ministerio Publico, A Traves De La Fiscalia De Asuntos Constitucionales, Amparos Y Exhibicion Personal" },
        { NOMBRE: "Diana Elizabeth Ralda Sandoval" },
        { NOMBRE: "Y Ministerio Público, A Través De La Fiscalía Distrital De Izabal" }
      ]
    };
  }

  //obtiene datos
  public getAntecedente(noBoleta: string) {
    return this.http.get<any>(this.baseUrl + 'boleta/' + noBoleta + "/validar")
      .pipe(
        retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      );
  }
  public getSaldoPension(caso: number, pin: number) {
    return this.http.get<any>(this.baseUrl + 'caso/' + caso + '/' + pin + '/consultar')
      .pipe(
        retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      );
  }
  public getCompetencias() {
    return this.http.get<any>(this.baseUrl + 'ANTEJUICIOS/getCompetencia')
      .pipe(
        retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      );
  }
  public getFase() {
    return this.http.get<any>(this.baseUrl + 'ANTEJUICIOS/getDescripcionPagina')
      .pipe(
        retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      );
  }
  public filterItems(searchTerm, dir) {
    return dir.filter((item) => {
      return item.NOMBRE_DEPENDENCIA.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }
  public getDirectorio() {
    return this.http.get<any>(this.baseUrl + 'Directorio/getDirectorio')
      .pipe(
        retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      );
  }
  public getDepartamentos() {
    return this.http.get<any>(this.dmzCIDEJDesa + 'SGT2/ObtenerDepartamentos')
      .pipe(
        retry(3), // retry a failed request up to 3 times
        catchError(this.handleError) // then handle the error
      );
  }
  public getUbicacionDependencias() {
    return this.http.get<any>(this.baseUrl + 'Directorio/getDirectorioEdificio')
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
  // obtener dependencias con latitud y longitud iguales
  public getDependenciasEdificio(lat, lng) {
    return this.http.get<any>(this.baseUrl + 'Directorio/getDependenciasUbicacion/longitud/' + lng + '/latitud/' + lat)
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
  }
  // obtener entidades de CAPE
  public getEntidades() {
    return this.http.get<any>(this.baseUrlCAPE + 'Catalogos/getEntidadAnonima')
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
  }
    // obtener series anónima
    public getSeriesAnonima() {
      return this.http.get<any>(this.baseUrlCAPE + 'Catalogos/getSerieAnonima')
        .pipe(
          retry(3),
          catchError(this.handleError)
        )
    }
  // send request Carencia
  public postRequestCarencia(requestCarencia) {
    return this.http.post<any>(this.baseUrlCAPE + 'GenerarCarencia/solicitudAnonimaApp/', requestCarencia)
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
  }

  // send request Carencia
  public ValidarBoleta(correlativo, llave) {
    return this.http.get<any>(this.baseUrlCAPE + 'Validar/validarCorrelativo/'+correlativo+'/validarLlave/'+llave)
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
  }

    // send request Carencia
    public getSolicitudValidaApp(solicitud) {
      return this.http.post<any>(this.baseUrlCAPE + 'Solicitudes/getSolicitudValidaApp',solicitud)
        .pipe(
          retry(3),
          catchError(this.handleError)
        )
    }
        // send request Carencia
    public getConstantesAnonimas() {
      return this.http.get<any>(this.baseUrlCAPE + 'Login/getConstantesAnonimas')
        .pipe(
          retry(3),
          catchError(this.handleError)
        )
    }
        // send request Carencia
        public getEstadosAnonimo() {
          return this.http.get<any>(this.baseUrlCAPE + 'Constancia/getEstadosAnonimo')
            .pipe(
              retry(3),
              catchError(this.handleError)
            )
        }
        // send request Carencia
        public getEstadoSolicitudAnonimo(idConstancia) {
          return this.http.get<any>(this.baseUrlCAPE + 'Constancia/estadoSolicitud/'+idConstancia+'/getEstadoSolicitudAnonimo')
            .pipe(
              retry(3),
              catchError(this.handleError)
            )
        }
  // send request Carencia
  public inconvenienteBoleta(boleta) {
    return this.http.post<any>(this.baseUrlCAPE + 'GenerarCarencia/insReciboInvalidoAnonimo', boleta)
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
  }

    // Obtener data de carencia de Antecedente Penal
    public getDataAntecedente(boleta) {
      return this.http.get<any>(this.baseUrlCAPE + 'Validar/consultarCarencia/'+boleta)
        .pipe(
          retry(3),
          catchError(this.handleError)
        )
    }

  // send request Carencia
  public getListaAntejuicios(antejuicio) {
    return this.http.post<any>(this.servicesCIDEJ + 'ANTEJUICIOS/listadoDeAntejuicios', antejuicio)
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
  }

  public getAnios() {
    return this.anios;
  }
  public getMunicipios(idDepartamento) {
    return this.http.get<any>(this.dmzCIDEJDesa + 'SGT2/Departamento/'+idDepartamento+'/ObtenerMunicipios')
    .pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError) // then handle the error
    );
  }
  public getDespachos(idMunicipio) {
    return this.http.get<any>(this.dmzCIDEJDesa + 'SGT2/UbicacionGeografica/'+idMunicipio+'/ObtenerDespachos')
    .pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError) // then handle the error
    );
  }
  public getDependencias() {
    return this.dependencias;
  }
  public getEstado() {
    return this.http.get<any>(this.dmzCIDEJDesa + 'SGT2/ObtenerEstadosAudiencia')
    .pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError) // then handle the error
    );
  }
  public getColumnas() {
    return this.columnas;
  }
  public getAudiencias(codigoDespacho, fechaInicio, fechaFinal,Estado) {
    return this.http.get<any>(this.dmzCIDEJDesa + 'Consulta/codigo_despacho/'+codigoDespacho+'/fecha_inicio/'+fechaInicio+'/fecha_final/'+fechaFinal+'/estado_audiencia/'+Estado+'/Get_Audiencias_Sgt1')
    .pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError) // then handle the error
    );
  }
  public getAmparo(no, anio) {
    return this.http.get<any>(this.dmzCIDEJDesa + 'AMPAROS/'+no+'/'+anio+'/muestraAmparos')
    .pipe(
      retry(3),
      catchError(this.handleError)
    )
  }


  // servicios ojAppUrl

  // get idCurrentUbicacion
  public getIdCurrentIdIntegracion() {
    return this.http.get<any>(this.ojAppUrl + 'IntegranteCSJ/currentId')
    .pipe(
      retry(3),
      catchError(this.handleError)
    )
  }

  // get idCurrentUbicacion
  public getIdCurrentIdUbicacion() {
    return this.http.get<any>(this.ojAppUrl + 'Ubicacion')
    .pipe(
      retry(3),
      catchError(this.handleError)
    )
  }

    // get idCurrentDependencia
  public getIdCurrentIdDependencia() {
    return this.http.get<any>(this.ojAppUrl + 'Dependencia')
    .pipe(
      retry(3),
      catchError(this.handleError)
    )
  }

  // get idCurrentContacto
  public getIdCurrentIdContacto() {
    return this.http.get<any>(this.ojAppUrl + 'updContacto')
    .pipe(
      retry(3),
      catchError(this.handleError)
    )
  }

    // get idCurrentContacto
    public getContactos() {
      return this.http.get<any>(this.ojAppUrl + 'Contacto')
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
    }
    // get idCurrentContacto
    public getIntegrantesCSJ() {
      return this.http.get<any>(this.ojAppUrl + 'IntegranteCSJ')
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
    }
    // get Home
    public getHome() {
      return this.http.get<any>(this.ojAppUrl + 'Home')
      .pipe(
        retry(3),
        catchError(this.handleError)
      )
    }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error.message); //gives the object object
      //this.showError(error.json());




    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  };
}
