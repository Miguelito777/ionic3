import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';
import * as crypto from 'crypto-js';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    public dateObj = new Date();
    public fecha256 : String;
  constructor() {
    this.fecha256 = crypto.SHA256("CIT-CAPE-D"+this.dateObj.getDate()+"-M"+this.dateObj.getMonth()+"-Y"+this.dateObj.getFullYear()).toString();
  }
 
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Get the auth token from the service.
    req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${this.fecha256}`//,
          //Origin:'wsdesacit.oj.gob.gt'
        }
      });
  
      return next.handle(req);
  }
}