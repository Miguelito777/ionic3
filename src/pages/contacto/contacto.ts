import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RestProvider } from '../../providers/rest/rest';
import { Platform } from 'ionic-angular';
import { AppAvailability } from '@ionic-native/app-availability';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { Ubicacion } from '../../pages/ubicacion/ubicacion';


/**
 * Generated class for the ContactoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contacto',
  templateUrl: 'contacto.html',
})
export class ContactoPage {
  private contactos : Array<Object>;
  public scheme: string = '';
  public home : any = {};


  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private toastCtrl: ToastController, private RestProvider: RestProvider, public loadingCtrl: LoadingController, public platform: Platform, public appAvailability: AppAvailability, private callNumber: CallNumber, private emailComposer: EmailComposer) {
    /*this.getHome();
    this.storage.get('idContacto').then((idContacto) =>{
      if(idContacto == null){
        this.getIdCurrentIdContacto();
        this.getContactos();
      }else{
        this.verifyIdCurrentContacto(idContacto);
      }
    });*/
  }
  private getHome(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getHome().subscribe(
      data => {
        loader.dismiss();
        this.storage.set('home', data.data[0]);
        this.getHomeStorage();
      },
      error => {
        loader.dismiss();
        this.getHomeStorage();
        console.log(error);
      }
    )
  }
  private getHomeStorage(){
    this.storage.get('home').then((home) => {
      if(home == null) {
        this.home = {
          direccion:"21 Calle 7-70 Zona 1. Centro Civico, Guatemala, Guatemala",
          slogan:"Por una justicia accesible, transparente, pronta y cumplida.",
          telefono:"PBX: 2290-4444",
          url:"www.oj.gob.gt",
          texto:"."
        }
      } else {
        this.home = home;
      }
    });
  }
  // get id of current integration
  private getIdCurrentIdContacto(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getIdCurrentIdContacto().subscribe(
      data => {
        loader.dismiss();
        this.storage.set('idContacto', data.tcUpdContacto.correlativo);
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    )
  }
  //Verifi current Integration
  private verifyIdCurrentContacto(idCurrentIdContacto){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getIdCurrentIdContacto().subscribe(
      data => {
        loader.dismiss();
        if(idCurrentIdContacto == data.tcUpdContacto.correlativo){
          this.storage.get('contactos').then((contactos) =>{
            this.contactos = JSON.parse(contactos);
          });
        }else{
          this.getIdCurrentIdContacto();
          this.getContactos();
        }
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        this.storage.get('contactos').then((contactos) =>{
          this.contactos = JSON.parse(contactos);
        });
        console.log(error);
      }
    )
  }
  private getContactos(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getContactos().subscribe(
      data => {
        loader.dismiss();
        this.contactos = data.data;
        this.storage.set('contactos', JSON.stringify(this.contactos));
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    )
  }
  presentToast(msj) {
    let toast = this.toastCtrl.create({
      message: msj,
      duration: 3000,
      position: 'middle'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
  isIOS() {
    if (this.platform.is('ios')) {
      return true;
    } else {
      return false;
    }
  }
  isAndroid() {
    if (this.platform.is('android')) {
      return true;
    } else {
      return false;
    }
  } 
  findSchemeFb() {
    if (this.isIOS()) {
      this.scheme = 'fb://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.facebook.katana';
      return this.scheme;
    }
  }
  findSchemeTw() {
    if (this.isIOS()) {
      this.scheme = 'twitter://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.twitter.android';
      return this.scheme;
    }
  }
  findSchemeYt() {
    if (this.isIOS()) {
      this.scheme = 'youtube://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.google.android.youtube';
      return this.scheme;
    }
  }
  openFacebookApp() {
    let esquema = this.findSchemeFb();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('fb://page/125498334137251', '_system');
        console.log('Facebook application available and opened');
      } else {
        window.open('https://www.facebook.com/organismojudicial.gt/', '_self');
        console.log('Facebook application not available, opened website in native browser');
      }
    })
  }
  openTwitterApp() {
    let esquema = this.findSchemeTw();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('twitter://user?screen_name=OJGuatemala', '_system');
        console.log('Twitter application available and opened');
      } else {
        window.open('https://twitter.com/OJGuatemala', '_self');
        console.log('Twitter application not available, opened website in native browser');
      }
    })
  }
  openYoutubeApp() {
    let esquema = this.findSchemeYt();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('https://www.youtube.com/user/OJGuatemala', '_system');
        console.log('Youtube application available and opened');
      } else {
        window.open('https://www.youtube.com/user/OJGuatemala', '_self');
        console.log('Youtube application not available, opened website in native browser');
      }
    })
  }
  public callThem(telefono){
    let number = telefono.split(' '); 
    let phoneNumber = number[1].split('-');
    this.callNumber.callNumber(phoneNumber[0]+phoneNumber[1], true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
  public callThemPBX(){
    /*let number = telefono.split(' '); 
    let phoneNumber = number[0].split(':');
    let numberPBX = phoneNumber[1].split('-');
    this.callNumber.callNumber(numberPBX[0]+numberPBX[1], true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));*/
    this.navCtrl.push(Ubicacion);
  }
  public sendEmail(motivo,descripcion,toEmail){
    let email = {
      to:toEmail,
      cc:[],
      bcc:[],
      attachment:[],
      subject:motivo,
      body:descripcion,
      isHtml:false
    }
    this.emailComposer.open(email);
  }
}
