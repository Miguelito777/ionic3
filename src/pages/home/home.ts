import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { AntecedentesPenales } from '../antecedentesPenales/antecedentesPenales';
import { PensionAlimenticia } from '../pensionAlimenticia/pensionAlimenticia';
import { Ubicacion } from '../ubicacion/ubicacion';
import { Directorio } from '../directorio/directorio';
import { Amparos } from '../amparos/amparos';
import { Antejuicios } from '../antejuicios/antejuicios';
import { Audiencias } from '../audiencias/audiencias';
import { Integracion } from '../csj/integracion';
import { Cape } from '../cape/cape'
import { AppAvailability } from '@ionic-native/app-availability';
import { Platform } from 'ionic-angular';
import { ContactoPage } from '../contacto/contacto';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  items = [];
  idIntegration:number;
  idDependencias:number;
  idContacto:number;
  idUbicacion:number;
  
  public scheme: string = '';
  public home : any = {};
  constructor(public navCtrl: NavController, public platform: Platform, public appAvailability: AppAvailability, private RestProvider: RestProvider, public loadingCtrl: LoadingController, private storage: Storage) {
    //this.getHome();
  }
  public changeToUbicacion() {
    //console.log("page Change");
    this.navCtrl.push(Ubicacion);
  }
  public changeToCSJ() {
    //console.log("page Change");
    this.navCtrl.push(Integracion);
  }
  public changeToAntecedentes() {
    //console.log("page Change");
    this.navCtrl.push(AntecedentesPenales);
  }
  public changeToPension() {
    //console.log("page Change");
    this.navCtrl.push(PensionAlimenticia);
  }
  public changeToDirectorio() {
    //console.log("page Change");
    this.navCtrl.push(Directorio);
  }
  public changeToAmparos() {
    //console.log("page Change");
    this.navCtrl.push(Amparos);
  }
  public changeToAntejuicios() {
    //console.log("page Change");
    this.navCtrl.push(Antejuicios);
  }
  public changeToAudiencias() {
    //console.log("page Change");
    this.navCtrl.push(Audiencias);
  }
  public changeToCAPEHome() {
    //console.log("page Change");
    this.navCtrl.push(Cape);
  }
  public changeToContacto(){
    this.navCtrl.push(ContactoPage);
  }
  isIOS() {
    if (this.platform.is('ios')) {
      return true;
    } else {
      return false;
    }
  }
  isAndroid() {
    if (this.platform.is('android')) {
      return true;
    } else {
      return false;
    }
  }
  findSchemeFb() {
    if (this.isIOS()) {
      this.scheme = 'fb://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.facebook.katana';
      return this.scheme;
    }
  }
  findSchemeTw() {
    if (this.isIOS()) {
      this.scheme = 'twitter://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.twitter.android';
      return this.scheme;
    }
  }
  findSchemeYt() {
    if (this.isIOS()) {
      this.scheme = 'youtube://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.google.android.youtube';
      return this.scheme;
    }
  }
  openFacebookApp() {
    let esquema = this.findSchemeFb();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('fb://page/125498334137251', '_system');
        console.log('Facebook application available and opened');
      } else {
        window.open('https://www.facebook.com/organismojudicial.gt/', '_self');
        console.log('Facebook application not available, opened website in native browser');
      }
    })
  }
  openTwitterApp() {
    let esquema = this.findSchemeTw();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('twitter://user?screen_name=OJGuatemala', '_system');
        console.log('Twitter application available and opened');
      } else {
        window.open('https://twitter.com/OJGuatemala', '_self');
        console.log('Twitter application not available, opened website in native browser');
      }
    })
  }
  openYoutubeApp() {
    let esquema = this.findSchemeYt();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('https://www.youtube.com/user/OJGuatemala', '_system');
        console.log('Youtube application available and opened');
      } else {
        window.open('https://www.youtube.com/user/OJGuatemala', '_self');
        console.log('Youtube application not available, opened website in native browser');
      }
    })
  }

  private getHome(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getHome().subscribe(
      data => {
        loader.dismiss();
        this.storage.set('home', data.data[0]);
        this.getHomeStorage();
      },
      error => {
        loader.dismiss();
        this.getHomeStorage();
        console.log(error);
      }
    )
  }
  private getHomeStorage(){
    this.storage.get('home').then((home) => {
      if(home == null) {
        this.home = {
          direccion:"21 Calle 7-70 Zona 1. Centro Civico, Guatemala, Guatemala",
          slogan:"Por una justicia accesible, transparente, pronta y cumplida.",
          telefono:"PBX: 2290-4444",
          url:"www.oj.gob.gt",
          texto:"."
        }
      } else {
        this.home = home;
      }
    });
  }
}
