import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Geolocation } from '@ionic-native/geolocation';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { CallNumber } from '@ionic-native/call-number';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-dependencia',
  templateUrl: 'dependencia.html',
  //styleUrls: ['app.directorio.css'],
})
export class Dependencia {
  private dependencyName;
  private dependencyAddress;
  private dependencyPhone;
  private dependencyPhoneFull;
  private dependencyImg;
  private currentLat;
  private currentLng
  private latDest;
  private lngDest;
  public home : any = {};

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, private navParams: NavParams, public geolocation: Geolocation, private launchNavigator: LaunchNavigator, private callNumber: CallNumber, private storage: Storage) {
    this.dependencyName = navParams.get("NOMBRE_DEPENDENCIA");
    this.dependencyAddress = navParams.get("DIRECCION");
    this.dependencyPhoneFull = navParams.get("TELEFONO_COMPLETO");
    this.dependencyPhone = navParams.get("TELEFONO");
    this.dependencyImg = navParams.get("img");
    this.latDest = navParams.get("LAT");
    this.lngDest = navParams.get("LNG");
    this.getHome();
  }
  private getHome(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getHome().subscribe(
      data => {
        loader.dismiss();
        this.storage.set('home', data.data[0]);
        this.getHomeStorage();
      },
      error => {
        loader.dismiss();
        this.getHomeStorage();
        console.log(error);
      }
    )
  }
  private getHomeStorage(){
    this.storage.get('home').then((home) => {
      if(home == null) {
        this.home = {
          direccion:"21 Calle 7-70 Zona 1. Centro Civico, Guatemala, Guatemala",
          slogan:"Por una justicia accesible, transparente, pronta y cumplida.",
          telefono:"PBX: 2290-4444",
          url:"www.oj.gob.gt",
          texto:"."
        }
      } else {
        this.home = home;
      }
    });
  }
  public leadMe() {
    //console.log(this.latDest+'-'+this.lngDest);
    // get current position
    this.geolocation.getCurrentPosition().then(pos => {
      this.currentLat = pos.coords.latitude;
      this.currentLng = pos.coords.longitude;
    });

    let currentPosition = this.currentLat + ", " + this.currentLng;
    this.launchNavigator.isAppAvailable(this.launchNavigator.APP.GOOGLE_MAPS);

    this.launchNavigator.navigate([this.latDest, this.lngDest], {
      start: currentPosition
    }).then(
      success => console.log('Launched navigator'),
      error => console.log('Error launching navigator', error)
    );
  }
  public callThem(){
    this.callNumber.callNumber(this.dependencyPhone, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
}
