import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams } from 'ionic-angular';
import { Actuaciones } from '../amparos/actuaciones';
import { InfoGeneral } from '../amparos/infoGeneral';
import { TercerosInteresados } from "../amparos/tercerosInteresados";

@Component({
  selector: 'page-amparo',
  templateUrl: 'amparo.html'
})
export class Amparo {
  public informacionGeneral:any;

    tab1Root = InfoGeneral;
    tab2Root = Actuaciones;
    tab3Root = TercerosInteresados;
    public amparo:any;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private navParams: NavParams) {
    this.amparo = navParams.data;
  }
}