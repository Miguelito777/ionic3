import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-tercerosInteresados',
  templateUrl: 'tercerosInteresados.html'
})
export class TercerosInteresados {
  public tercerosInteresados:any;
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private navParams: NavParams) {
    this.tercerosInteresados = this.navParams.data.TERCEROS_INTERESADOS;
  }
  public countTercerosInteresados(){
    return this.tercerosInteresados.length;
  }
}