import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams  } from 'ionic-angular';

@Component({
  selector: 'page-amparos',
  templateUrl: 'actuaciones.html'
})
export class Actuaciones {
  public actuaciones:any;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private navParams: NavParams) {
    this.actuaciones = this.navParams.data.ACTUACIONES;
  }
  public countActuaciones(){
    return this.actuaciones.length;
  }
}