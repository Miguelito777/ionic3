import { Component } from '@angular/core';
import { LoadingController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-infoGeneral',
  templateUrl: 'infoGeneral.html'
})
export class InfoGeneral {
  public informacionGeneral:any;

  constructor(private navParams: NavParams) {
    this.informacionGeneral = this.navParams.data.INFORMACION_GENERAL;
  }
  
}
