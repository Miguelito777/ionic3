import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { SolicitarCarencia } from '../../pages/cape/solicitarCarencia';
import { ValidarCAPE } from '../../pages/cape/validarCAPE';
import { Storage } from '@ionic/storage';
import { Seguimiento } from '../../pages/cape/seguimiento';
import { scanQR } from '../../pages/cape/scanQR';

@Component({
  selector: 'page-cape',
  templateUrl: 'cape.html',
  //styleUrls: ['app.directorio.css'],
})
export class Cape {
  public cripto : any;
  public home : any = {};
  
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, private storage: Storage) {
    this.getHome();
  }

  public getStatusRequest(){
    this.navCtrl.push(SolicitarCarencia,{});
  }
  public validingCAPE(){
    this.navCtrl.push(ValidarCAPE,{});
  }
  public goToSeguimiento(){
    this.navCtrl.push(Seguimiento,{});
  }
  public goToScannQR(){
    this.navCtrl.push(scanQR,{});
  }
  private getHome(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getHome().subscribe(
      data => {
        loader.dismiss();
        this.storage.set('home', data.data[0]);
        this.getHomeStorage();
      },
      error => {
        loader.dismiss();
        this.getHomeStorage();
        console.log(error);
      }
    )
  }
  private getHomeStorage(){
    this.storage.get('home').then((home) => {
      if(home == null) {
        this.home = {
          direccion:"21 Calle 7-70 Zona 1. Centro Civico, Guatemala, Guatemala",
          slogan:"Por una justicia accesible, transparente, pronta y cumplida.",
          telefono:"PBX: 2290-4444",
          url:"www.oj.gob.gt",
          texto:"."
        }
      } else {
        this.home = home;
      }
    });
  }
}