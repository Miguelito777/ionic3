import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController, NavParams, ToastController  } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RequestWithout } from './giveRequestWithout';
import { InvalidRecipt } from './invalidRecipt';
@Component({
  selector: 'page-solicitarCarencia',
  templateUrl: 'solicitarCarencia.html',
  //styleUrls: ['app.directorio.css'],
})
export class SolicitarCarencia {
    myForm: FormGroup;
    public entidades : Array<String>;
    public series : Array<String>;
    public requestWithout : any;
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, public formBuilder: FormBuilder, public modalCtrl: ModalController, private toastCtrl: ToastController) {
    this.myForm = this.createMyForm();
    this.requestWithout = {};  
        let loader = this.loadingCtrl.create({});
        loader.present();
        this.RestProvider.getEntidades().subscribe(
          data => {
            this.entidades = data;
            this.RestProvider.getSeriesAnonima().subscribe(
              data => {
                loader.dismiss();
                this.series = data;
              },
              error => {
                console.log(error);
              }
            )
          },
          error => {
            console.log(error);
          }
        )
}
  private createMyForm(){
    return this.formBuilder.group({
      dpi: ['', Validators.required],
      dateBorn: ['', Validators.required],
      bank: ['', Validators.required],
      recibo: ['', Validators.required],
      dateRecibo: ['', Validators.required],
      });
  }

  public sendRequestWithout(){
    let requestWithoutObject = {
      CUI : this.requestWithout.CUI,
      ID_ENTIDAD:this.requestWithout.ID_ENTIDAD,
      ID_SERIE:this.requestWithout.ID_SERIE,
      DOCUMENTO : this.requestWithout.DOCUMENTO,
      FECHA_NACIMIENTO : new Date(),
      FECHA : new Date()
    };
    let dateBorn = this.getDateObject(this.requestWithout.FECHA_NACIMIENTO);
    let date = this.getDateObject(this.requestWithout.FECHA);
    requestWithoutObject.FECHA_NACIMIENTO = dateBorn;
    requestWithoutObject.FECHA = date;

    let loader = this.loadingCtrl.create({
    });
    loader.present();
    this.RestProvider.postRequestCarencia(requestWithoutObject).subscribe(
      data => {
        loader.dismiss();
        //this.entidades = data;
        if(data.result == 'OK'){
          this.giveRequest(data.msj);
          let toast = this.toastCtrl.create({
            message: 'Boleta Solicitada',
            duration: 3000,
            position: 'middle'
          });
          toast.present();
        }else{
          if(data.id == -2){
            let toast = this.toastCtrl.create({
              message: 'Recibo ya ingresado',
              duration: 3000,
              position: 'middle'
            });
            toast.present();
          }else if(data.id == -3){
            let toast = this.toastCtrl.create({
              message: 'Boleta Invalida',
              duration: 3000,
              position: 'middle'
            });
            toast.present();
            this.navCtrl.push(InvalidRecipt,{});
          }else{
            let toast = this.toastCtrl.create({
              message: 'Favor de verificar sus datos',
              duration: 3000,
              position: 'middle'
            });
            toast.present(); 
          }
        }
      },
      error => {
        console.log(error);
      }
    );
  }
  
  // Function to convert one date type '2018-05-31' to date object
  private getDateObject(date) : Date{
    let dateArray = date.split('-');
    let day = parseInt(dateArray[2]);
    let mont = parseInt(dateArray[1])-1;
    let year = parseInt(dateArray[0]);
    let dateParse = new Date(year,mont,day);
    return dateParse;
  }
  private giveRequest(correlativoRequest){
    
    this.navCtrl.push(RequestWithout,{ message: correlativoRequest });
    //let profileModal = this.modalCtrl.create(RequestWithout, { message: mensaje });
    //profileModal.present();

  }
}