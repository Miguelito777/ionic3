import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { SolicitarCarencia } from '../../pages/cape/solicitarCarencia';
import { ValidarCAPE } from '../../pages/cape/validarCAPE';
import { Storage } from '@ionic/storage';
import { Seguimiento } from '../../pages/cape/seguimiento';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

@Component({
  selector: 'page-cape',
  templateUrl: 'scanQR.html',
  //styleUrls: ['app.directorio.css'],
})
export class scanQR {
  public cripto : any;
  public home : any = {};
  
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, private storage: Storage, private qrScanner: QRScanner) {
    this.getHome();
    window.document.querySelector('ion-app').classList.add('transparentBody')
    
    // Pedir permiso de utilizar la camara
  this.qrScanner.prepare().then((status: QRScannerStatus) => {
    if (status.authorized) {
      // el permiso fue otorgado
      // iniciar el escaneo
      let scanSub = this.qrScanner.scan().subscribe((texto: string) => {
        console.log('Scanned something', texto);
        
        this.qrScanner.hide(); // esconder el preview de la camara
        scanSub.unsubscribe(); // terminar el escaneo
      }); 
      this.qrScanner.resumePreview();
            // show camera preview
            this.qrScanner.show()
            .then((data : QRScannerStatus)=> { 
              console.log('datashowing', data.showing);
              //alert(data.showing);
            },err => {
              //alert(err);
      
            });
    } else if (status.denied) {
      // el permiso no fue otorgado de forma permanente
      // debes usar el metodo QRScanner.openSettings() para enviar el usuario a la pagina de configuracion
      // desde ahí podrán otorgar el permiso de nuevo
    } else {
      // el permiso no fue otorgado de forma temporal. Puedes pedir permiso de en cualquier otro momento
    }
  }) .catch((e: any) => console.log('El error es: ', e));
  }

  ionViewDidLeave() {
    window.document.querySelector('ion-app').classList.remove('transparentBody')
  }
  private getHome(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getHome().subscribe(
      data => {
        loader.dismiss();
        this.storage.set('home', data.data[0]);
        this.getHomeStorage();
      },
      error => {
        loader.dismiss();
        this.getHomeStorage();
        console.log(error);
      }
    )
  }
  private getHomeStorage(){
    this.storage.get('home').then((home) => {
      if(home == null) {
        this.home = {
          direccion:"21 Calle 7-70 Zona 1. Centro Civico, Guatemala, Guatemala",
          slogan:"Por una justicia accesible, transparente, pronta y cumplida.",
          telefono:"PBX: 2290-4444",
          url:"www.oj.gob.gt",
          texto:"."
        }
      } else {
        this.home = home;
      }
    });
  }
}