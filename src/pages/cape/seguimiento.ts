import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController, NavParams, ToastController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SeguimientoTimeLine } from './SeguimientoTimeLine';


@Component({
    selector: 'page-seguimiento',
    templateUrl: 'seguimiento.html',
    //styleUrls: ['app.directorio.css'],
})
export class Seguimiento {
    myForm: FormGroup;
    cape: any;
    constantes : any;
    estados : any;
    events : any;
    ESTADO_SISTEMA : any;
    ESTADO : any;
    elemento : any;
    JUSTIFICACION : any;
    seguimientoData:any;
    //public entidades : Array<String>;
    
    constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, public formBuilder: FormBuilder, public modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.myForm = this.createMyForm();
        this.cape = {};
        this.constantes = {};
        this.estados = {};
    }
    private createMyForm() {
        return this.formBuilder.group({
            cui: ['', Validators.required],
            noSolicitud: ['', Validators.required],
        });
    }
    public getStatus() {



        let loader = this.loadingCtrl.create({});
        loader.present();
        this.RestProvider.getSolicitudValidaApp(this.cape).subscribe(
            data => {

                //loader.dismiss();
                //this.entidades = data;
                //console.log(data.id);
                //this.navCtrl.push(BoletaCarencia,data);

                //loader.dismiss();
                //this.entidades = data;
                //console.log(data);
                //this.navCtrl.push(BoletaCarencia,data);








                if (data.result == 'OK') {



                    this.RestProvider.getConstantesAnonimas().subscribe(
                        data => {
                          //loader.dismiss();
                          //this.storage.set('home', data.data[0]);
                          //this.getHomeStorage();
                          this.constantes = data;
                        },
                        error => {
                          //loader.dismiss();
                          //this.getHomeStorage();
                          console.log(error);
                        }
                      )


      




                    let ID_SOLICITUD_RESP = data.id;






                    this.RestProvider.getEstadosAnonimo().subscribe(
                        data => {
                            this.estados = data;
                            this.events = [];
                            var j = 0;





                    this.RestProvider.getEstadoSolicitudAnonimo(ID_SOLICITUD_RESP).subscribe(
                        data => {

                            this.ESTADO_SISTEMA = data.ID_ESTADO_SISTEMA;
                            this.ESTADO = data.ID_ESTADO;
                            console.log(this.ESTADO_SISTEMA);
                            console.log(this.ESTADO);
                            for (var i = 0; i < this.estados.length; i++) {
                              j = i + 1; //debido a los estados se deben saltar 2
                              //console.log(response.data);
                              if (data.ID_ESTADO_SISTEMA > j) {
                                this.elemento = {
                                  badgeClass: 'success',
                                  style: 'green',
                                  icon: 'checkbox-outline',
                                  title: j + ' - ' + this.estados[i].ESTADO_SISTEMA,
                                  content: this.estados[i].DESCRIPCION
                                };
                              } else if (data.ID_ESTADO_SISTEMA < j) {
                                this.elemento = {
                                  badgeClass: 'default',
                                  style: 'gray',
                                  icon: 'checkbox-outline',
                                  title: j + ' - ' + this.estados[i].ESTADO_SISTEMA,
                                  content: this.estados[i].DESCRIPCION
                                };
                              } else {
                                if (data.ID_ESTADO == this.constantes.ESTADO_INACTIVO) {
                                  this.elemento = {
                                  badgeClass: 'error',
                                  style: 'red',
                                  icon: 'create',
                                  title: j + ' - ' + this.estados[i].ESTADO_SISTEMA,
                                  content: this.estados[i].DESCRIPCION
                                };
                                if (data.ID_ESTADO_SISTEMA == this.constantes.ESTADO_AUTORIZACION_BOLETA) {
                                // $scope.elemento.tooltip = "Esta solicitud fue descartada por Gerencia Financiera. Comuníquese con ellos para mas información."
                                this.elemento.tooltip = data.JUSTIFICACION;
                                this.JUSTIFICACION =  data.data.JUSTIFICACION;}
                                else{
                                this.elemento.tooltip = "Se encontró que posee antecedentes penales, comuníquese con la unidad de archivo si considera que es un error."
                                this.elemento.title= j + ' - ' + this.estados[i].OTRO_NOMBRE;
                                }
                                }
                                else{
        
                                this.elemento = {
                                  badgeClass: 'background-color:warning;',
                                  style: 'orange',
                                  icon: 'create',
                                  title: j + ' - ' + this.estados[i].ESTADO_SISTEMA,
                                  content: this.estados[i].DESCRIPCION
                                };
        
        
                                if (data.ID_ESTADO_SISTEMA == this.constantes.ESTADO_IDENTIFICACION_HOMONIMO) {
                                  this.elemento.tooltip = "Se detecto que cuenta con un homónimo. Su solicitud tomara un poco mas de tiempo en lo que se analiza este caso."
                                };
        
                                }
        
                              }
                              this.events.push(this.elemento);
                            }
                            //console.log(this.events);
                            loader.dismiss();
                            this.navCtrl.push(SeguimientoTimeLine,{
                                events:this.events,
                                idEstadoSolicitud:this.ESTADO_SISTEMA,
                                ID_SOLICITUD_DIALOGO:ID_SOLICITUD_RESP,
                                constantes:this.constantes
                            });
                        },
                        error => {
                          console.log(error);
                        }
                      )


                      

                            /*$scope.dialogShowGrupo = ngDialog.open({
                              template: 'app/constancia/constancia.timeline.tpl.html',
                              //scope: $scope,
                              showClose: true,
                              closeByEscape: true,
                              scope: $scope,
                              controller: ['$scope', 'constanciaService', 'authService', function($scope, constanciaService, authService) {
                                $scope.ID_SOLICITUD_DIALOGO = ID_SOLICITUD_RESP;
                                $scope.descargarCarencia = function(ID_SOLICITUD) {
            
                                    var passPhrase = "CIT-" + appSettings.Sistema + "-M" + new Date().getMinutes() + "-H" + new Date().getHours() + "-D" + new Date().getDate() + "-M" + new Date().getMonth() + "-Y" + new Date().getFullYear();
                                    var p_autorizacion = $cryptoOJ.encryptUrl(passPhrase, ID_SOLICITUD+"");
            
                                    window.open(appSettings.restApiServiceBaseUri + 'Constancia/Solicitud/' + ID_SOLICITUD + '/Usuario/' + authService.getIdUsuario() + '/validarAutorizacion/' + p_autorizacion + '/downloadArchivo', '_blank');
            
                                }
                                $scope.descargarFichaAntecedentes = function(ID_SOLICITUD) {
            
                                  var passPhrase = "CIT-" + appSettings.Sistema + "-M" + new Date().getMinutes() + "-H" + new Date().getHours() + "-D" + new Date().getDate() + "-M" + new Date().getMonth() + "-Y" + new Date().getFullYear();
                                  var p_autorizacion = $cryptoOJ.encryptUrl(passPhrase, ID_SOLICITUD+"");
            
                                  window.open(appSettings.restApiServiceBaseUri + 'Constancia/Solicitud/' + ID_SOLICITUD + '/Usuario/' + authService.getIdUsuario() + '/validarAutorizacion/' + p_autorizacion + '/downloadFichaAntecedentes', '_blank');
            
                              }
                              }],
                            });*/
                        },
                        error => {
                          //loader.dismiss();
                          //this.getHomeStorage();
                          console.log(error);
                        }
                      )






      
                  } else {
                    //toastr.error(response.data.msj);
                    console.log(data.msj);
                  }
                  this.seguimientoData = {};






            },
            error => {
                console.log(error);
            }
        );










    }
}