import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController, NavParams, ToastController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'page-boletaCarencia',
    templateUrl: 'boletaCarencia.html',
    //styleUrls: ['app.directorio.css'],
})
export class BoletaCarencia {
    public consultaCarencia:any = {};
    public VERIFICACIONREALIZADA : any = {};
    public LIMITE:any;
    public VENCIDA:any;

    constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, public formBuilder: FormBuilder, public modalCtrl: ModalController, private toastCtrl: ToastController, private navParams : NavParams) {
        if (navParams.data.VENCIMIENTO==1){
            this.consultaCarencia.MAXIMO_VERIFICAR=navParams.data.MAXIMO_VERIFICAR;
            if((navParams.data.NUMERO_VERIFICACIONES)<=this.consultaCarencia.MAXIMO_VERIFICAR){
            this.VERIFICACIONREALIZADA=true;
            this.consultaCarencia.CORRELATIVO=navParams.data.CORRELATIVO;
            this.consultaCarencia.NOMBRE_COMPLETO=navParams.data.NOMBRE_COMPLETO;
            this.consultaCarencia.VIGENCIA=navParams.data.VIGENCIA;
            this.consultaCarencia.NUMERO_VERIFICACIONES=navParams.data.NUMERO_VERIFICACIONES;
            this.consultaCarencia.FECHA_CREACION=navParams.data.FECHA_CREACION;
            this.consultaCarencia.FIRMA_INTERNA=navParams.data.FIRMA_INTERNA;
          }
            else{
            this.LIMITE=true;
            }
          }
          else{
            this.VENCIDA=true;
          }


    }

}