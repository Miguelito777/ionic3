import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController, NavParams, ToastController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'page-invalidRecipt',
    templateUrl: 'invalidRecipt.html',
    //styleUrls: ['app.directorio.css'],
})
export class InvalidRecipt {
    myForm: FormGroup;
    public invalidRecipt: any;

    constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, public formBuilder: FormBuilder, public modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.invalidRecipt = {};
        this.myForm = this.createMyForm();
    }
    private createMyForm() {
        return this.formBuilder.group({
            cui: ['', Validators.required],
            tel: ['', Validators.required],
            dateBorn: ['', Validators.required],
            dateTransaction: ['', Validators.required],
            bank: ['', Validators.required],
            recibo: ['', Validators.required],
        });
    }
    public sendIssue() {

        let requestWithoutObject = {
            CUI: this.invalidRecipt.CUI,
            ABREVIATURA: this.invalidRecipt.ABREVIATURA,
            DOCUMENTO: this.invalidRecipt.DOCUMENTO,
            FECHA_NACIMIENTO: new Date(),
            FECHA_TRANSACCION_BANCARIA: new Date()
        };
        let dateBord = this.getDateObject(this.invalidRecipt.FECHA_NACIMIENTO);
        let date = this.getDateObject(this.invalidRecipt.FECHA_TRANSACCION_BANCARIA);
        requestWithoutObject.FECHA_NACIMIENTO = dateBord;
        requestWithoutObject.FECHA_TRANSACCION_BANCARIA = date;



        let loader = this.loadingCtrl.create({
        });
        loader.present();
        this.RestProvider.inconvenienteBoleta(requestWithoutObject).subscribe(
            data => {
                loader.dismiss();
                //this.entidades = data;
                console.log(data);
                //this.navCtrl.push(BoletaCarencia,data);
                if (data.result == 'OK') {
                    //this.giveRequest(data.msj);
                    let toast = this.toastCtrl.create({
                        message: 'Problema reportadp'+data.msj,
                        duration: 3000,
                        position: 'middle'
                    });
                    toast.present();
                }
            },
            error => {
                console.log(error);
            }
        );
    }
    private getDateObject(date): Date {
        let dateArray = date.split('-');
        let day = parseInt(dateArray[2]);
        let mont = parseInt(dateArray[1]) - 1;
        let year = parseInt(dateArray[0]);
        let dateParse = new Date(year, mont, day);
        return dateParse;
    }
}