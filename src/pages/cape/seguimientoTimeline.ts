import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as crypto from 'crypto-js';
import { RestProvider } from '../../providers/rest/rest';

@Component({
  selector: 'page-seguimientoTimeline',
  templateUrl: 'seguimientoTimeline.html'
})
export class SeguimientoTimeLine {
    public events:any;
    public ESTADO_SISTEMA:any;
    public ID_SOLICITUD_RESP:any;
    public constantes : any;
  items = [
    {
      title: 'Courgette daikon',
      content: 'Parsley amaranth tigernut silver beet maize fennel spinach. Ricebean black-eyed pea maize scallion green bean spinach cabbage jícama bell pepper carrot onion corn plantain garbanzo. Sierra leone bologi komatsuna celery peanut swiss chard silver beet squash dandelion maize chicory burdock tatsoi dulse radish wakame beetroot.',
      icon: 'calendar',
      time: {subtitle: '4/16/2013', title: '21:30'}
    },
    {
      title: 'Courgette daikon',
      content: 'Parsley amaranth tigernut silver beet maize fennel spinach. Ricebean black-eyed pea maize scallion green bean spinach cabbage jícama bell pepper carrot onion corn plantain garbanzo. Sierra leone bologi komatsuna celery peanut swiss chard silver beet squash dandelion maize chicory burdock tatsoi dulse radish wakame beetroot.',
      icon: 'calendar',
      time: {subtitle: 'January', title: '29'}
    },
    {
      title: 'Courgette daikon',
      content: 'Parsley amaranth tigernut silver beet maize fennel spinach. Ricebean black-eyed pea maize scallion green bean spinach cabbage jícama bell pepper carrot onion corn plantain garbanzo. Sierra leone bologi komatsuna celery peanut swiss chard silver beet squash dandelion maize chicory burdock tatsoi dulse radish wakame beetroot.',
      icon: 'calendar',
      time: {title: 'Short Text'}
    }
  ]
  constructor(public navCtrl: NavController, private navParams: NavParams, private RestProvider: RestProvider) {
    this.events = navParams.get("events");
    this.ESTADO_SISTEMA = navParams.get("idEstadoSolicitud");
    this.ID_SOLICITUD_RESP = navParams.get("ID_SOLICITUD_DIALOGO");
    this.constantes = navParams.get("constantes");
    //console.log(this.ESTADO_SISTEMA);
    //console.log(this.constantes);
    
}
  public getAntecedente(){
    var passPhrase = "CIT-CAPE-M" + new Date().getMinutes() + "-H" + new Date().getHours() + "-D" + new Date().getDate() + "-M" + new Date().getMonth() + "-Y" + new Date().getFullYear();
    var p_autorizacion = crypto.SHA256(passPhrase).toString();
    window.open(this.RestProvider.baseUrlCAPE + 'Constancia/Solicitud/' + this.ID_SOLICITUD_RESP + '/Usuario/1/validarAutorizacion/' + p_autorizacion + '/downloadArchivo', '_blank');
    console.log("No hize nada");
}
}