import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController, NavParams, ToastController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BoletaCarencia } from './boletaCarencia';
@Component({
    selector: 'page-validarCAPE',
    templateUrl: 'validarCAPE.html',
    //styleUrls: ['app.directorio.css'],
})
export class ValidarCAPE {
    myForm: FormGroup;
    cape: any;
    public entidades : Array<String>;
    
    constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, public formBuilder: FormBuilder, public modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.myForm = this.createMyForm();
        this.cape = {};
        let loader = this.loadingCtrl.create({
        });
        loader.present();
        this.RestProvider.getEntidades().subscribe(
          data => {
            loader.dismiss();
            this.entidades = data;
          },
          error => {
            console.log(error);
          }
        )


    }
    private createMyForm() {
        return this.formBuilder.group({
            boleta: ['', Validators.required],
            llave: ['', Validators.required],
        });
    }
    public validingCAPE() {
        let loader = this.loadingCtrl.create({
        });
        loader.present();
        this.RestProvider.ValidarBoleta(this.cape.BOLETA, this.cape.LLAVE).subscribe(
            data => {

                //loader.dismiss();
                //this.entidades = data;
                console.log(data.id);
                //this.navCtrl.push(BoletaCarencia,data);

                this.RestProvider.getDataAntecedente(data.id).subscribe(
                    data => {
                        loader.dismiss();
                        //this.entidades = data;
                        console.log(data);
                        this.navCtrl.push(BoletaCarencia,data);
                    },
                    error => {
                        console.log(error);
                    }
                );

            },
            error => {
                console.log(error);
            }
        );
    }
}