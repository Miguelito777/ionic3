import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
//import { DatePipe } from '@angular/common';
import { RestProvider } from '../../providers/rest/rest';
import { AppAvailability } from '@ionic-native/app-availability';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-antecedentesPenales',
  templateUrl: 'antecedentesPenales.html'
})
export class AntecedentesPenales {
  boleta = {};
  public scheme: string = '';
  public home : any = {};

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, private toastCtrl: ToastController, public platform: Platform, public appAvailability: AppAvailability, private storage: Storage) {
    this.getHomeStorage();
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Consultando Saldo...",
      duration: 3000
    });
    loader.present();
  }

  public getAntecedente(noBoleta : string){
    noBoleta = noBoleta.trim();
    if (!noBoleta) {
      return;
    }else{
      let loader = this.loadingCtrl.create({
        content: "Consultando boleta...",
        //duration: 1000
      });
      loader.present();
      this.RestProvider.getAntecedente(noBoleta).subscribe(
        data => {
          loader.dismiss();
          this.boleta = data;
        },
        error => {
          loader.dismiss();
          this.presentToast("Error de conexión");
          console.log(error);
        }
      )
    }
  }
  public getStatusRequest(){
    
  }
  presentToast(msj) {
    let toast = this.toastCtrl.create({
      message: msj,
      duration: 3000,
      position: 'middle'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
  isIOS() {
    if (this.platform.is('ios')) {
      return true;
    } else {
      return false;
    }
  }
  isAndroid() {
    if (this.platform.is('android')) {
      return true;
    } else {
      return false;
    }
  }
  findSchemeFb() {
    if (this.isIOS()) {
      this.scheme = 'fb://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.facebook.katana';
      return this.scheme;
    }
  }
  findSchemeTw() {
    if (this.isIOS()) {
      this.scheme = 'twitter://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.twitter.android';
      return this.scheme;
    }
  }
  findSchemeYt() {
    if (this.isIOS()) {
      this.scheme = 'youtube://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.google.android.youtube';
      return this.scheme;
    }
  }
  openFacebookApp() {
    let esquema = this.findSchemeFb();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('fb://page/125498334137251', '_system');
        console.log('Facebook application available and opened');
      } else {
        window.open('https://www.facebook.com/organismojudicial.gt/', '_self');
        console.log('Facebook application not available, opened website in native browser');
      }
    })
  }
  openTwitterApp() {
    let esquema = this.findSchemeTw();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('twitter://user?screen_name=OJGuatemala', '_system');
        console.log('Twitter application available and opened');
      } else {
        window.open('https://twitter.com/OJGuatemala', '_self');
        console.log('Twitter application not available, opened website in native browser');
      }
    })
  }
  openYoutubeApp() {
    let esquema = this.findSchemeYt();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('https://www.youtube.com/user/OJGuatemala', '_system');
        console.log('Youtube application available and opened');
      } else {
        window.open('https://www.youtube.com/user/OJGuatemala', '_self');
        console.log('Youtube application not available, opened website in native browser');
      }
    })
  }
  private getHomeStorage(){
    this.storage.get('home').then((home) => {
      if(home == null) {
        this.home = {
          direccion:"21 Calle 7-70 Zona 1. Centro Civico, Guatemala, Guatemala",
          slogan:"Por una justicia accesible, transparente, pronta y cumplida.",
          telefono:"PBX: 2290-4444",
          url:"www.oj.gob.gt",
          texto:"."
        }
      } else {
        this.home = home;
      }
    });
  }
}
