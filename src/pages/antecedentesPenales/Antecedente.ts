export class Antecedente {
   NUMERO_DOCUMENTO: number;
   FECHA: string;
   NUMERO: number;
   TIPO_DOCUMENTO: number;
   NOMBRES: string;
   SEGUNDOAPELLIDO: string;
   PRIMERAPELLIDO: string;
   constructor(values: Object = {}) {
        Object.assign(this, values);
   }
}
