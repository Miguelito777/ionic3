import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
//import { DatePipe } from '@angular/common';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-penal',
  templateUrl: 'penal.html'
})
export class Penal {
  integracionCamaraPenal = [];
  integracionCamaraPenalOrder = [];
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, private storage: Storage) {
    this.storage.get('integrantes').then((integrantes) => {
      let integracionCamaraPenal = JSON.parse(integrantes);
      for(var i in integracionCamaraPenal){
        if(integracionCamaraPenal[i].idCamara == 1){
          this.integracionCamaraPenal.push(integracionCamaraPenal[i]);
        }
      }
      let j = 1;
      while(j <= this.integracionCamaraPenal.length){
        for(var i in this.integracionCamaraPenal){
          if(this.integracionCamaraPenal[i].presidente == j){
            this.integracionCamaraPenalOrder.push(this.integracionCamaraPenal[i]);
          }
        }
        j++;
      }
    });   
  }
}