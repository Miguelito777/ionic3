import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-civil',
  templateUrl: 'civil.html'
})
export class Civil {
  integracionCamaraCivil = [];
  integracionCamaraCivilOrder = [];
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private storage: Storage) {
    this.storage.get('integrantes').then((integrantes) => {
      let integracionCamaraCivil = JSON.parse(integrantes);
      for(var i in integracionCamaraCivil){
        if(integracionCamaraCivil[i].idCamara == 2){
          this.integracionCamaraCivil.push(integracionCamaraCivil[i]);
        }
      }
      let j = 1;
      while(j <= this.integracionCamaraCivil.length){
        for(var i in this.integracionCamaraCivil){
          if(this.integracionCamaraCivil[i].presidente == j){
            this.integracionCamaraCivilOrder.push(this.integracionCamaraCivil[i]);
          }
        }
        j++;
      }
    }); 
  }
}