import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
//import { DatePipe } from '@angular/common';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-integracion',
  templateUrl: 'csj.html'
})
export class csj {
  integracionCSJ = [];
  private integrantes:any;
  
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, private storage: Storage, private toastCtrl: ToastController) {
    //this.storage.remove('idIntegracion');
    //this.storage.remove('integrantes');
    //console.log("Saldos ");
    this.storage.get('idIntegracion').then((idIntegracion) => {
      if (idIntegracion == null) {
        this.getIdCurrentIdIntegracion();
        this.getCurrentIntegration();
      } else {
        this.verifyIdCurrentIntegration(idIntegracion);
      }
    });  
  }
  // get id of current integration
  private getIdCurrentIdIntegracion(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getIdCurrentIdIntegracion().subscribe(
      data => {
        loader.dismiss();
        this.storage.set('idIntegracion', data.tcUpdIntegracion.correlativo);
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    )
  }
  //Verifi current Integration
  private verifyIdCurrentIntegration(idCurrentIdIntegracion){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getIdCurrentIdIntegracion().subscribe(
      data => {
        loader.dismiss();
        if(idCurrentIdIntegracion == data.tcUpdIntegracion.correlativo){
          this.storage.get('integrantes').then((integrantes) => {
            let integracionCSJTemp = JSON.parse(integrantes);
            let presidente;
            for(var i in integracionCSJTemp){
              if(integracionCSJTemp[i].presidente == 0){
                presidente = integracionCSJTemp[i];
                integracionCSJTemp.splice(i,1);
              }
            }
            integracionCSJTemp.unshift(presidente);
            this.integracionCSJ = integracionCSJTemp;
          });  
        }else{
          this.getIdCurrentIdIntegracion();
          this.getCurrentIntegration();
        }
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    )
  }
  // get current integration
  private getCurrentIntegration(){
    let loader = this.loadingCtrl.create({
      content: 'Obteniendo Integracion...'
    });
    loader.present();
    this.RestProvider.getIntegrantesCSJ()
    .subscribe(
      data => {
        loader.dismiss();
        this.integrantes = data.data;
        //console.log(this.integrantes);
        this.storage.set('integrantes', JSON.stringify(this.integrantes));
        this.storage.get('integrantes').then((integrantes) => {
          //console.log(JSON.parse(integrantes));
          let integracionCSJTemp = JSON.parse(integrantes);
          let presidente;
          //console.log(integracionCSJTemp);
          for(var i in integracionCSJTemp){
            if(integracionCSJTemp[i].presidente == 0){
              presidente = integracionCSJTemp[i];
              integracionCSJTemp.splice(i,1);
            }
          }
          integracionCSJTemp.unshift(presidente);
          this.integracionCSJ = integracionCSJTemp;
        });  
      },error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        this.storage.get('integrantes').then((integrantes) => {
          let integracionCSJTemp = JSON.parse(integrantes);
          let presidente;
          for(var i in integracionCSJTemp){
            if(integracionCSJTemp[i].presidente == 0){
              presidente = integracionCSJTemp[i];
              integracionCSJTemp.splice(i,1);
            }
          }
          integracionCSJTemp.unshift(presidente);
          this.integracionCSJ = integracionCSJTemp;
        });  
      }
    )
     
  }
  presentToast(msj) {
    let toast = this.toastCtrl.create({
      message: msj,
      duration: 3000,
      position: 'middle'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
}