import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
//import { DatePipe } from '@angular/common';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-amparoAntejuicio',
  templateUrl: 'amparoAntejuicio.html'
})
export class AmparoAntejuicio {
  integracionCamaraAmparoAntejuicio = [];
  integracionCamaraAmparoAntejuicioOrder = [];
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private storage: Storage) {
    this.storage.get('integrantes').then((integrantes) => {
      let integracionCamaraAmparoAntejuicio = JSON.parse(integrantes);
      for(var i in integracionCamaraAmparoAntejuicio){
        if(integracionCamaraAmparoAntejuicio[i].idCamara == 3){
          this.integracionCamaraAmparoAntejuicio.push(integracionCamaraAmparoAntejuicio[i]);
        }
      }
      let j = 1;
      while(j <= this.integracionCamaraAmparoAntejuicio.length){
        for(var i in this.integracionCamaraAmparoAntejuicio){
          if(this.integracionCamaraAmparoAntejuicio[i].presidente == j){
            this.integracionCamaraAmparoAntejuicioOrder.push(this.integracionCamaraAmparoAntejuicio[i]);
          }
        }
        j++;
      }
    }); 
  }
}