import { Component } from '@angular/core';
import { NavController, Content, LoadingController, ModalController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { ListAudiencias } from '../../pages/audiencias/listAudiencias';
import { AppAvailability } from '@ionic-native/app-availability';
import { Platform } from 'ionic-angular';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-audiencias',
  templateUrl: 'audiencias.html'
})
export class Audiencias {
  myForm: FormGroup;
  public departamentos: Array<Object> = [];
  public municipios: Array<Object> = [];
  public despachos: Array<Object> = [];
  public estados: Array<Object> = [];
  public scheme: string = '';
  public home: any = {};
  public despachoSelect: any = {};

  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, public loadingCtrl: LoadingController, private RestProvider: RestProvider, public modalCtrl: ModalController, private toastCtrl: ToastController, public platform: Platform, public appAvailability: AppAvailability, private storage: Storage) {
    this.myForm = this.createMyForm();

    let loader = this.loadingCtrl.create({});
    this.RestProvider.getDepartamentos().subscribe(
      data => {
        this.departamentos = data;
        this.RestProvider.getEstado().subscribe(
          data => {
            loader.dismiss();
            let newStatus = {
              ID_ESTADO_AGENDA: 0,
              ESTADO_AGENDA: 'Todos'
            }
            data.push(newStatus);
            this.estados = data;
            //this.estados.push(newStatus);
          },
          error => {
            loader.dismiss();
            this.presentToast("Error de conexión");
            console.log(error);
          }
        )
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    );
    this.getHomeStorage();
  }
  private createMyForm() {
    return this.formBuilder.group({
      departamento: ['', Validators.required],
      municipio: ['', Validators.required],
      despacho: ['', Validators.required],
      estado: ['', Validators.required]
    });
  }
  public changeDepartament() {
    let loader = this.loadingCtrl.create({
      content: 'Municipios...'
    });
    loader.present();
    this.RestProvider.getMunicipios(this.myForm.value.departamento).subscribe(
      data => {
        loader.dismiss();
        this.municipios = data;
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    )
  }
  public changeMunicipio() {
    let loader = this.loadingCtrl.create({
      content: 'Municipios...'
    });
    loader.present();
    this.RestProvider.getDespachos(this.myForm.value.municipio).subscribe(
      data => {
        loader.dismiss();
        this.despachos = data;
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    )
  }
  public getAudiencias() {
    let despacho = {
      idDespacho: 0,
      estado: ''
    };
    despacho.idDespacho = this.despachoSelect.ID_DESPACHO;
    despacho.estado = this.myForm.value.estado;
    this.navCtrl.push(ListAudiencias, despacho);
  }
  presentToast(msj) {
    let toast = this.toastCtrl.create({
      message: msj,
      duration: 3000,
      position: 'middle'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  isIOS() {
    if (this.platform.is('ios')) {
      return true;
    } else {
      return false;
    }
  }
  isAndroid() {
    if (this.platform.is('android')) {
      return true;
    } else {
      return false;
    }
  }
  findSchemeFb() {
    if (this.isIOS()) {
      this.scheme = 'fb://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.facebook.katana';
      return this.scheme;
    }
  }
  findSchemeTw() {
    if (this.isIOS()) {
      this.scheme = 'twitter://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.twitter.android';
      return this.scheme;
    }
  }
  findSchemeYt() {
    if (this.isIOS()) {
      this.scheme = 'youtube://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.google.android.youtube';
      return this.scheme;
    }
  }
  openFacebookApp() {
    let esquema = this.findSchemeFb();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('fb://page/125498334137251', '_system');
        console.log('Facebook application available and opened');
      } else {
        window.open('https://www.facebook.com/organismojudicial.gt/', '_self');
        console.log('Facebook application not available, opened website in native browser');
      }
    })
  }
  openTwitterApp() {
    let esquema = this.findSchemeTw();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('twitter://user?screen_name=OJGuatemala', '_system');
        console.log('Twitter application available and opened');
      } else {
        window.open('https://twitter.com/OJGuatemala', '_self');
        console.log('Twitter application not available, opened website in native browser');
      }
    })
  }
  openYoutubeApp() {
    let esquema = this.findSchemeYt();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('https://www.youtube.com/user/OJGuatemala', '_system');
        console.log('Youtube application available and opened');
      } else {
        window.open('https://www.youtube.com/user/OJGuatemala', '_self');
        console.log('Youtube application not available, opened website in native browser');
      }
    })
  }
  private getHomeStorage() {
    this.storage.get('home').then((home) => {
      if (home == null) {
        this.home = {
          direccion: "21 Calle 7-70 Zona 1. Centro Civico, Guatemala, Guatemala",
          slogan: "Por una justicia accesible, transparente, pronta y cumplida.",
          telefono: "PBX: 2290-4444",
          url: "www.oj.gob.gt",
          texto: "."
        }
      } else {
        this.home = home;
      }
    });
  }
}
