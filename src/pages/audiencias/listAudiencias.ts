import { Component } from '@angular/core';
import { NavController, Content, LoadingController, ModalController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

import { Subject } from 'rxjs/Subject';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import {
  CalendarEvent,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';

import { DemoComponent } from '../../modules/angular-calendar/component';


@Component({
  selector: 'page-listAudiencias',
  templateUrl: 'listAudiencias.html'
})
export class ListAudiencias {
  private audiencias:any;
  public home : any = {};
  viewDate: Date = new Date();
  view = 'week';
  locale: string = 'es-GT';
  isDragging = false;
  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
    /*{
      start: addHours(startOfDay(new Date()), 7),
      end: addHours(startOfDay(new Date()), 9),
      title: 'First Event',
      cssClass: 'custom-event',
      color: {
        primary: '#488aff',
        secondary: '#bbd0f5'
      },
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: addHours(startOfDay(new Date()), 10),
      end: addHours(startOfDay(new Date()), 12),
      title: 'Second Event',
      cssClass: 'custom-event',
      color: {
        primary: '#488aff',
        secondary: '#bbd0f5'
      },
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    }*/
  ];

  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, public loadingCtrl: LoadingController, private RestProvider: RestProvider, public modalCtrl: ModalController, private alertCtrl: AlertController, private navParams: NavParams, private storage: Storage) {
    this.navCtrl.push(DemoComponent, navParams.data);
    this.getHomeStorage();
    //console.log(navParams.data);
    /*this.audiencias = this.RestProvider.getAudiencias();
    for(var i in this.audiencias){
      var eventDate = this.audiencias[i].Fecha.split('-');
      var event = new Date(eventDate[2],eventDate[1]-1,eventDate[0]);
      var timeStart = this.audiencias[i].horaInicio.split(':');
      var time = parseInt(timeStart[0]);
      //console.log(eventDate[2]+' * '+(eventDate[1]-1)+' * '+eventDate[0]);
      this.events.push(
        {
          start: addHours(startOfDay(event), time),
          end: addHours(startOfDay(event), time+1),
          title: this.audiencias[i].descripcion,
          cssClass: 'custom-event',
          color: {
            primary: '#488aff',
            secondary: '#bbd0f5'
          },
          resizable: {
            beforeStart: true,
            afterEnd: true
          },
          draggable: true
        }
      );
    }*/
  }
  private detailsAudiencia(audiencia){
    console.log(audiencia);
  }

  handleEvent(event: CalendarEvent): void {
    let alert = this.alertCtrl.create({
      title: event.title,
      message: 'Descripcion de la Audiencia',//event.start + ' to ' + event.end,
      buttons: ['OK']
    });
    alert.present();
  }
  eventTimesChanged({event, newStart, newEnd} : CalendarEventTimesChangedEvent): void {
    if (this.isDragging) {
      return;
    }
    this.isDragging = true;
 
    event.start = newStart;
    event.end = newEnd;
    //this.refresh.next(); //For mov event times
 
    setTimeout(() => {
      this.isDragging = false;
    },1000);
  }
  hourSegmentClicked(event): void {
    let newEvent: CalendarEvent = {
      start: event.date,
      end: addHours(event.date, 1),
      title: 'TEST EVENT',
      cssClass: 'custom-event',
      color: {
        primary: '#488aff',
        secondary: '#bbd0f5'
      },
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    }
 
    this.events.push(newEvent);
    //this.refresh.next(); //Para gregar nuevo evento donde de click el usuariio
  }
  private getHomeStorage(){
    this.storage.get('home').then((home) => {
      if(home == null) {
        this.home = {
          direccion:"21 Calle 7-70 Zona 1. Centro Civico, Guatemala, Guatemala",
          slogan:"Por una justicia accesible, transparente, pronta y cumplida.",
          telefono:"PBX: 2290-4444",
          url:"www.oj.gob.gt",
          texto:"."
        }
      } else {
        this.home = home;
      }
    });
  }
}