import { Component } from '@angular/core';
import { NavController, ModalController, Platform, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Dependencia } from '../dependencia/dependencia';

@Component({
    selector: 'page-edificios',
    templateUrl: 'edificios.html',
    //styleUrls: ['app.directorio.css'],
  })
export class Edificio {
  public edificios:any;

    constructor(public platform: Platform, public params: NavParams, public viewCtrl: ViewController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, public navCtrl: NavController,) {
      let loader = this.loadingCtrl.create({});
      loader.present();
      this.RestProvider.getDependenciasEdificio(params.get('lat'),params.get('lng')).subscribe(
        data => {
          loader.dismiss();
          this.edificios = data.data;
        },error => {
          console.log(error);
        }
      )
    }
    dismiss() {
      this.viewCtrl.dismiss();
    }
    public goToDependencie(item){
      this.navCtrl.push(Dependencia, {
        NOMBRE_DEPENDENCIA: item.NOMBRE_DEPENDENCIA,
        DIRECCION: item.DIRECCION,
        TELEFONO: item.TELEFONO,
        img: 'dependency.png'
      });
    }
  }