import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController, NavParams, ToastController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { RestProvider } from '../../providers/rest/rest';
import { Edificio } from '../edificios/edificios';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';
import { AppAvailability } from '@ionic-native/app-availability';

@Component({
  selector: 'page-ubicacion',
  templateUrl: 'ubicacion.html',
  //styleUrls: ['app.directorio.css'],
})
export class Ubicacion {
  markers = [
  ]
  // google maps zoom level
  zoom: number = 14;
  myWidth: number = 180;
  public origin;
  public destination;
  public dir;
  // initial center position for the map
  public lat: number;
  public lng: number;
  private edificios: any;
  public scheme: string = '';
  public home : any = {};



  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public geolocation: Geolocation, private RestProvider: RestProvider, public modalCtrl: ModalController, private launchNavigator: LaunchNavigator, private storage: Storage, private toastCtrl: ToastController, public platform: Platform, public appAvailability: AppAvailability) {    
    // get current position
    geolocation.getCurrentPosition().then(pos => {
      this.markers.push({
        lat: pos.coords.latitude,
        lng: pos.coords.longitude,
        label: '',
        draggable: false,
        title: 'Posicion Actual',
        icon: 'assets/imgs/aca.png',
        description: 'Posicion Actual'
      });

      this.markers.push({
        lat: pos.coords.latitude+0.008,
        lng: pos.coords.longitude+0.008,
        label: '',
        draggable: false,
        title: 'Estamos para servirle',
        icon: 'assets/imgs/femicidio.png',
        description: '77661897'
      });

      this.markers.push({
        lat: pos.coords.latitude-0.008,
        lng: pos.coords.longitude-0.008,
        label: '',
        draggable: false,
        title: 'Servicio de Calidad',
        icon: 'assets/imgs/laboral.png',
        description: '47896321'
      });

      this.lat = pos.coords.latitude;
      this.lng = pos.coords.longitude;
    });
    /*this.storage.get('idUbicacion').then((idUbicacion) => {
      if (idUbicacion == null) {
        this.getIdCurrentUbicacion();
        this.getEdificios();
      } else {
        this.verifyIdCurrentUbicacion(idUbicacion);
      }
    });
    this.getHomeStorage();*/
  }
  // get id of current Ubicacion
  private getIdCurrentUbicacion() {
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getIdCurrentIdUbicacion().subscribe(
      data => {
        loader.dismiss();
        this.storage.set('idUbicacion', data.tcUpdUbicacion.correlativo);
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    )
  }
  private verifyIdCurrentUbicacion(idCurrentIdUbicacion) {
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getIdCurrentIdUbicacion().subscribe(
      data => {
        loader.dismiss();
        if (idCurrentIdUbicacion == data.tcUpdUbicacion.correlativo) {
          this.storage.get('edificios').then((edificios) => {
            this.edificios = JSON.parse(edificios);
            this.addMarkets(this.edificios);
          });
        } else {
          this.getIdCurrentUbicacion();
          this.getEdificios();
        }
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        this.storage.get('edificios').then((edificios) => {
          this.edificios = JSON.parse(edificios);
          this.addMarkets(this.edificios);
        });
        console.log(error);
      }
    )
  }
  //Verifi current Integration

  public weGo(dependencia) {
    let currentPosition = this.lat + ", " + this.lng;
    this.launchNavigator.isAppAvailable(this.launchNavigator.APP.GOOGLE_MAPS);
    this.launchNavigator.navigate([dependencia.lat, dependencia.lng], {
      start: currentPosition
    }).then(
      success => console.log('Launched navigator'),
      error => console.log('Error launching navigator', error)
    );
  }

  public getDetailsEdificio(edificio) {
    let modal = this.modalCtrl.create(Edificio, {
      lat: edificio.lat,
      lng: edificio.lng
    });
    modal.present();
  }
  clickedMarker(label: string, index: number) {
    //console.log(`clicked the marker: ${label || index}`);
    //console.log(label);
    //console.log(index);
    /*for(var i in this.markers){
      console.log(this.markers[i]);
    }*/
    //console.log('get dependencias edificio');
  }
  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
  public goToDependencie(dependencie) {
    this.dir = {
      origin: { lat: this.lat, lng: this.lng },
      destination: { lat: dependencie.lat, lng: dependencie.lng }
    }
  }

  private getEdificios() {
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getUbicacionDependencias().subscribe(
      data => {
        loader.dismiss();
        this.edificios = data.data;
        this.storage.set('edificios', JSON.stringify(this.edificios));
        this.addMarkets(this.edificios);
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    );
  }

  private addMarkets(markets) {
    for (var i in markets) {
      this.markers.push({
        lat: +markets[i].LATITUD,
        lng: +markets[i].LONGITUD,
        label: '',
        draggable: false,
        title: markets[i].NOMBRE_DEPENDENCIA,
        icon: 'assets/imgs/' + markets[i].ICONO,
        description: markets[i].DIRECCION + ' Tel. ' + markets[i].TELEFONO + ' Cantidad Dependencias ' + markets[i].CANTIDAD_DEPENDENCIAS,
        cantidadDependencias: markets[i].CANTIDAD_DEPENDENCIAS
      });
    }
  }

  presentToast(msj) {
    let toast = this.toastCtrl.create({
      message: msj,
      duration: 3000,
      position: 'middle'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  isIOS() {
    if (this.platform.is('ios')) {
      return true;
    } else {
      return false;
    }
  }
  isAndroid() {
    if (this.platform.is('android')) {
      return true;
    } else {
      return false;
    }
  }
  findSchemeFb() {
    if (this.isIOS()) {
      this.scheme = 'fb://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.facebook.katana';
      return this.scheme;
    }
  }
  findSchemeTw() {
    if (this.isIOS()) {
      this.scheme = 'twitter://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.twitter.android';
      return this.scheme;
    }
  }
  findSchemeYt() {
    if (this.isIOS()) {
      this.scheme = 'youtube://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.google.android.youtube';
      return this.scheme;
    }
  }
  openFacebookApp() {
    let esquema = this.findSchemeFb();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('fb://page/125498334137251', '_system');
        console.log('Facebook application available and opened');
      } else {
        window.open('https://www.facebook.com/organismojudicial.gt/', '_self');
        console.log('Facebook application not available, opened website in native browser');
      }
    })
  }
  openTwitterApp() {
    let esquema = this.findSchemeTw();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('twitter://user?screen_name=OJGuatemala', '_system');
        console.log('Twitter application available and opened');
      } else {
        window.open('https://twitter.com/OJGuatemala', '_self');
        console.log('Twitter application not available, opened website in native browser');
      }
    })
  }
  openYoutubeApp() {
    let esquema = this.findSchemeYt();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('https://www.youtube.com/user/OJGuatemala', '_system');
        console.log('Youtube application available and opened');
      } else {
        window.open('https://www.youtube.com/user/OJGuatemala', '_self');
        console.log('Youtube application not available, opened website in native browser');
      }
    })
  }
  private getHomeStorage(){
    this.storage.get('home').then((home) => {
      if(home == null) {
        this.home = {
          direccion:"21 Calle 7-70 Zona 1. Centro Civico, Guatemala, Guatemala",
          slogan:"Por una justicia accesible, transparente, pronta y cumplida.",
          telefono:"PBX: 2290-4444",
          url:"www.oj.gob.gt",
          texto:"."
        }
      } else {
        this.home = home;
      }
    });
  }
}
// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
