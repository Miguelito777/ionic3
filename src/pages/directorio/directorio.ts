import { Component } from '@angular/core';
import { NavController, LoadingController,ToastController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Dependencia } from '../dependencia/dependencia';
import { Storage } from '@ionic/storage';
import { AppAvailability } from '@ionic-native/app-availability';
import { Platform } from 'ionic-angular';

@Component({
  selector: 'page-directorio',
  templateUrl: 'directorio.html',
  //styleUrls: ['app.directorio.css'],
})
export class Directorio {
  searchTerm: string = '';
  public directorioJdos: any;
  public filtroDirectorio: any;
  public scheme: string = '';
  
  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private RestProvider: RestProvider, private storage: Storage, private toastCtrl: ToastController, public platform: Platform, public appAvailability: AppAvailability) {
    this.storage.get('idDependencia').then((idDependencia) => {
      if(idDependencia == null){
        this.getIdCurrentIdDependencia();
        this.getDirectorio();
      }else{
        this.verifyIdCurrentDependencia(idDependencia);
      }
    })
  }
  // get id of current integration
  private getIdCurrentIdDependencia(){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getIdCurrentIdDependencia().subscribe(
      data => {
        loader.dismiss();
        this.storage.set('idDependencia', data.tcUpdDependencia.correlativo);
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    )
  }
  //Verifi current Integration
  private verifyIdCurrentDependencia(idCurrentIdDependencia){
    let loader = this.loadingCtrl.create({});
    loader.present();
    this.RestProvider.getIdCurrentIdDependencia().subscribe(
      data => {
        loader.dismiss();
        if(idCurrentIdDependencia == data.tcUpdDependencia.correlativo){
          this.storage.get('directorio').then((directorio) =>{
            this.directorioJdos = JSON.parse(directorio);
            this.setFilteredItems();
          });        
        }else{
          this.getIdCurrentIdDependencia();
          this.getDirectorio();
        }
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        this.storage.get('directorio').then((directorio) =>{
          this.directorioJdos = JSON.parse(directorio);
          this.setFilteredItems();
        });  
        console.log(error);
      }
    )
  }
  setFilteredItems() {
    this.filtroDirectorio = this.RestProvider.filterItems(this.searchTerm, this.directorioJdos);        
  }
  public goDetailsJdo(Jdo){
    this.navCtrl.push(Dependencia, {
      NOMBRE_DEPENDENCIA: Jdo.NOMBRE_DEPENDENCIA,
      DIRECCION: Jdo.DIRECCION,
      TELEFONO_COMPLETO: Jdo.TELEFONO_COMPLETO,
      TELEFONO: Jdo.TELEFONO,
      LAT:Jdo.LATITUD,
      LNG:Jdo.LONGITUD,
      img: 'dependency.png'
    });
  }

  private getDirectorio(){
    //this.directorioJdos = RestProvider.directorioJdos;
    let loader = this.loadingCtrl.create({
      content: 'Obteniendo Directorio...'
    });
    loader.present();
    this.RestProvider.getDirectorio().subscribe(
      data => {
        loader.dismiss();
        this.directorioJdos = data.data;
        this.storage.set('directorio', JSON.stringify(this.directorioJdos));
        this.setFilteredItems();
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    )
  }
  presentToast(msj) {
    let toast = this.toastCtrl.create({
      message: msj,
      duration: 3000,
      position: 'middle'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  isIOS() {
    if (this.platform.is('ios')) {
      return true;
    } else {
      return false;
    }
  }
  isAndroid() {
    if (this.platform.is('android')) {
      return true;
    } else {
      return false;
    }
  }
  findSchemeFb() {
    if (this.isIOS()) {
      this.scheme = 'fb://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.facebook.katana';
      return this.scheme;
    }
  }
  findSchemeTw() {
    if (this.isIOS()) {
      this.scheme = 'twitter://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.twitter.android';
      return this.scheme;
    }
  }
  findSchemeYt() {
    if (this.isIOS()) {
      this.scheme = 'youtube://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.google.android.youtube';
      return this.scheme;
    }
  }

  openFacebookApp() {
    let esquema = this.findSchemeFb();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('fb://page/125498334137251', '_system');
        console.log('Facebook application available and opened');
      } else {
        window.open('https://www.facebook.com/organismojudicial.gt/', '_self');
        console.log('Facebook application not available, opened website in native browser');
      }
    })
  }
  openTwitterApp() {
    let esquema = this.findSchemeTw();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('twitter://user?screen_name=OJGuatemala', '_system');
        console.log('Twitter application available and opened');
      } else {
        window.open('https://twitter.com/OJGuatemala', '_self');
        console.log('Twitter application not available, opened website in native browser');
      }
    })
  }
  openYoutubeApp() {
    let esquema = this.findSchemeYt();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('https://www.youtube.com/user/OJGuatemala', '_system');
        console.log('Youtube application available and opened');
      } else {
        window.open('https://www.youtube.com/user/OJGuatemala', '_self');
        console.log('Youtube application not available, opened website in native browser');
      }
    })
  }
  
}