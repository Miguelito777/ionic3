import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { Antejuicio } from '../../pages/antejuicios/antejuicio';
import { AppAvailability } from '@ionic-native/app-availability';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-antejuicios',
  templateUrl: 'antejuicios.html'
})
export class Antejuicios {
  myForm: FormGroup;
  public competencias;
  public fases;
  public anios: any;
  public scheme: string = '';
  public home : any = {};
  
  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, public loadingCtrl: LoadingController, private RestProvider: RestProvider, private toastCtrl: ToastController, public platform: Platform, public appAvailability: AppAvailability, private storage: Storage) {
    this.getHomeStorage();
    this.myForm = this.createMyForm();
    let loader = this.loadingCtrl.create();
    loader.present();
    this.RestProvider.getCompetencias().subscribe(
      data => {
        this.competencias = data;
        this.RestProvider.getFase().subscribe(
          data => {
            loader.dismiss();
            this.fases = data;
          },
          error => {
            loader.dismiss();
            this.presentToast("Error de conexión");
            console.log(error);
          }
        )
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    )
    this.anios = this.RestProvider.getAnios();
  }
  public saveData(){
    let loader = this.loadingCtrl.create();
    loader.present();
    this.RestProvider.getListaAntejuicios(this.myForm.value).subscribe(
      data => {
        loader.dismiss();
        this.navCtrl.push(Antejuicio, data);
      },
      error => {
        loader.dismiss();
        this.presentToast("Error de conexión");
        console.log(error);
      }
    );
  }
  private createMyForm(){
    return this.formBuilder.group({
      AUTORIDAD_IMPUGNADA: [''],
      INTERPONENTE: [''],
      ANIO: [''],
      ANTEJUICIO: [''],
      FECHA_INICIO: [''],
      FECHA_FIN: [''],
      COMPETENCIA: [''],
      FASE_ENVIADA: ['']
    });
  }
  presentToast(msj) {
    let toast = this.toastCtrl.create({
      message: msj,
      duration: 3000,
      position: 'middle'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  isIOS() {
    if (this.platform.is('ios')) {
      return true;
    } else {
      return false;
    }
  }
  isAndroid() {
    if (this.platform.is('android')) {
      return true;
    } else {
      return false;
    }
  }
  findSchemeFb() {
    if (this.isIOS()) {
      this.scheme = 'fb://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.facebook.katana';
      return this.scheme;
    }
  }
  findSchemeTw() {
    if (this.isIOS()) {
      this.scheme = 'twitter://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.twitter.android';
      return this.scheme;
    }
  }
  findSchemeYt() {
    if (this.isIOS()) {
      this.scheme = 'youtube://';
      return this.scheme;
    } else if (this.isAndroid()) {
      this.scheme = 'com.google.android.youtube';
      return this.scheme;
    }
  }
  openFacebookApp() {
    let esquema = this.findSchemeFb();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('fb://page/125498334137251', '_system');
        console.log('Facebook application available and opened');
      } else {
        window.open('https://www.facebook.com/organismojudicial.gt/', '_self');
        console.log('Facebook application not available, opened website in native browser');
      }
    })
  }
  openTwitterApp() {
    let esquema = this.findSchemeTw();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('twitter://user?screen_name=OJGuatemala', '_system');
        console.log('Twitter application available and opened');
      } else {
        window.open('https://twitter.com/OJGuatemala', '_self');
        console.log('Twitter application not available, opened website in native browser');
      }
    })
  }
  openYoutubeApp() {
    let esquema = this.findSchemeYt();
    this.appAvailability.check(esquema).then(isApp => {
      if (isApp) {
        window.open('https://www.youtube.com/user/OJGuatemala', '_system');
        console.log('Youtube application available and opened');
      } else {
        window.open('https://www.youtube.com/user/OJGuatemala', '_self');
        console.log('Youtube application not available, opened website in native browser');
      }
    })
  }
  private getHomeStorage(){
    this.storage.get('home').then((home) => {
      if(home == null) {
        this.home = {
          direccion:"21 Calle 7-70 Zona 1. Centro Civico, Guatemala, Guatemala",
          slogan:"Por una justicia accesible, transparente, pronta y cumplida.",
          telefono:"PBX: 2290-4444",
          url:"www.oj.gob.gt",
          texto:"."
        }
      } else {
        this.home = home;
      }
    });
  }
}
