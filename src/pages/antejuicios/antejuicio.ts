import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-antejuicio',
  templateUrl: 'antejuicio.html'
})
export class Antejuicio {
  public antejuicios:any;
  public home : any = {};
  
  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, public loadingCtrl: LoadingController, private RestProvider: RestProvider, private navParams : NavParams, private storage: Storage) {
    //console.log(navParams);
    this.antejuicios = navParams.data;  
    this.getHomeStorage(); 
  }
  private getHomeStorage(){
    this.storage.get('home').then((home) => {
      if(home == null) {
        this.home = {
          direccion:"21 Calle 7-70 Zona 1. Centro Civico, Guatemala, Guatemala",
          slogan:"Por una justicia accesible, transparente, pronta y cumplida.",
          telefono:"PBX: 2290-4444",
          url:"www.oj.gob.gt",
          texto:"."
        }
      } else {
        this.home = home;
      }
    });
  }
}